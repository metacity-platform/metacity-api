<?php
/**
 * <API - Metacity>
 * Copyright (C) 2020.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Tests\Http\Services;


use App\Http\Services\ServiceService;
use App\service;
use App\user;
use Illuminate\Http\Request;
use Tests\TestCase;

class ServiceServiceTest extends TestCase
{
    public function testAddServiceServices()
    {
        $service = "Test";
        $description = "Service de test";
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();
        $request["service"] = $service;
        $request["desc"] = $description;

        $result = service::where("service", $service)->where('description', $description)->get();
        $this->assertEmpty($result);

        ServiceService::addServiceServices($request);
        $result = service::where("service", $service)->where('description', $description)->get();
        $this->assertNotEmpty($result);

        service::where("service", $service)->where('description', $description)->delete();
        $result = service::where("service", $service)->where('description', $description)->get();

        $this->assertEmpty($result);
    }

    public function testUpdateServiceServices()
    {
        $name = "Test";
        $newName = "Test2";
        $description = "Service de test";
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();
        $request["service"] = $name;
        $request["newName"] = $newName;
        $request["desc"] = $description;

        $result = service::where("service", $name)->where('description', $description)->get();
        $this->assertEmpty($result);

        $service = new Service();
        $service["service"] = $name;
        $service["description"] = $description;
        $service->save();
        $result = service::where("service", $name)->where('description', $description)->get();
        $this->assertNotEmpty($result);

        ServiceService::updateServiceServices($request);
        $result = service::where("service", $newName)->where('description', $description)->get();
        $this->assertNotEmpty($result);
        $result = service::where("service", $name)->where('description', $description)->get();
        $this->assertEmpty($result);

        service::where("service", $newName)->where('description', $description)->delete();
        $result = service::where("service", $newName)->where('description', $description)->get();

        $this->assertEmpty($result);
    }

    public function testDelServiceServices()
    {
        $name = "Test";
        $newName = service::first()["name"];
        $description = "Service de test";
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();


        $result = service::where("service", $name)->where('description', $description)->get();
        $this->assertEmpty($result);

        $service = new Service();
        $service["service"] = $name;
        $service["description"] = $description;
        $service->save();
        $result = service::where("service", $name)->where('description', $description)->get();
        $this->assertNotEmpty($result);

        ServiceService::delServiceServices($request, $name, $newName);
        $result = service::where("service", $name)->where('description', $description)->get();

        $this->assertEmpty($result);
    }

    public function testGetAllServicesServices()
    {
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();
        $result = ServiceService::getAllServicesServices();
        $themes = Service::get();
        $this->assertEquals($themes, $result);
    }
}
