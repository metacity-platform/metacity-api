<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatasetHasRepresentationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('dataset_has_representations')->delete();

        DB::table('dataset_has_representations')->insert(array(
            0 =>
                array(
                    'datasetId' => 1,
                    'representationName' => 'Graphique en barres',
                    'created_at' => '2019-11-27 13:48:13',
                    'updated_at' => '2019-11-27 13:48:13',
                ),
            1 =>
                array(
                    'datasetId' => 1,
                    'representationName' => 'Graphique en colonnes',
                    'created_at' => '2019-11-27 13:48:13',
                    'updated_at' => '2019-11-27 13:48:13',
                ),
            2 =>
                array(
                    'datasetId' => 1,
                    'representationName' => 'Graphique polaire',
                    'created_at' => '2019-11-27 13:48:13',
                    'updated_at' => '2019-11-27 13:48:13',
                ),
            3 =>
                array(
                    'datasetId' => 1,
                    'representationName' => 'Somme éléments',
                    'created_at' => '2019-11-27 13:48:13',
                    'updated_at' => '2019-11-27 13:48:13',
                ),
            4 =>
                array(
                    'datasetId' => 1,
                    'representationName' => 'Tableau',
                    'created_at' => '2019-11-27 13:48:13',
                    'updated_at' => '2019-11-27 13:48:13',
                ),
            5 =>
                array(
                    'datasetId' => 2,
                    'representationName' => 'Carte',
                    'created_at' => '2019-11-26 16:17:14',
                    'updated_at' => '2019-11-26 16:17:14',
                ),
            6 =>
                array(
                    'datasetId' => 2,
                    'representationName' => 'Somme éléments',
                    'created_at' => '2019-11-26 16:20:16',
                    'updated_at' => '2019-11-26 16:20:16',
                ),
            7 =>
                array(
                    'datasetId' => 2,
                    'representationName' => 'Tableau',
                    'created_at' => '2019-11-26 16:17:14',
                    'updated_at' => '2019-11-26 16:17:14',
                ),
            8 =>
                array(
                    'datasetId' => 3,
                    'representationName' => 'Carte',
                    'created_at' => '2019-11-27 15:11:20',
                    'updated_at' => '2019-11-27 15:11:20',
                ),
            9 =>
                array(
                    'datasetId' => 3,
                    'representationName' => 'Graphique en colonnes',
                    'created_at' => '2019-11-27 15:11:20',
                    'updated_at' => '2019-11-27 15:11:20',
                ),
            10 =>
                array(
                    'datasetId' => 3,
                    'representationName' => 'Tableau',
                    'created_at' => '2019-11-27 15:11:20',
                    'updated_at' => '2019-11-27 15:11:20',
                ),
            11 =>
                array(
                    'datasetId' => 4,
                    'representationName' => 'Carte',
                    'created_at' => '2019-11-27 15:10:02',
                    'updated_at' => '2019-11-27 15:10:02',
                ),
            12 =>
                array(
                    'datasetId' => 4,
                    'representationName' => 'Graphique en colonnes',
                    'created_at' => '2019-11-27 15:10:02',
                    'updated_at' => '2019-11-27 15:10:02',
                ),
            13 =>
                array(
                    'datasetId' => 4,
                    'representationName' => 'Somme éléments',
                    'created_at' => '2019-11-27 15:10:02',
                    'updated_at' => '2019-11-27 15:10:02',
                ),
            14 =>
                array(
                    'datasetId' => 4,
                    'representationName' => 'Tableau',
                    'created_at' => '2019-11-27 15:10:02',
                    'updated_at' => '2019-11-27 15:10:02',
                ),
            15 =>
                array(
                    'datasetId' => 5,
                    'representationName' => 'Graphique en anneau',
                    'created_at' => '2019-11-27 15:24:52',
                    'updated_at' => '2019-11-27 15:24:52',
                ),
            16 =>
                array(
                    'datasetId' => 5,
                    'representationName' => 'Graphique en colonnes',
                    'created_at' => '2019-11-27 15:24:52',
                    'updated_at' => '2019-11-27 15:24:52',
                ),
            17 =>
                array(
                    'datasetId' => 5,
                    'representationName' => 'Graphique en courbes',
                    'created_at' => '2019-11-27 15:24:52',
                    'updated_at' => '2019-11-27 15:24:52',
                ),
            18 =>
                array(
                    'datasetId' => 5,
                    'representationName' => 'Graphique en secteur',
                    'created_at' => '2019-11-27 15:24:52',
                    'updated_at' => '2019-11-27 15:24:52',
                ),
            19 =>
                array(
                    'datasetId' => 5,
                    'representationName' => 'Somme éléments',
                    'created_at' => '2019-11-27 15:24:52',
                    'updated_at' => '2019-11-27 15:24:52',
                ),
            20 =>
                array(
                    'datasetId' => 5,
                    'representationName' => 'Tableau',
                    'created_at' => '2019-11-27 15:24:52',
                    'updated_at' => '2019-11-27 15:24:52',
                ),
            21 =>
                array(
                    'datasetId' => 7,
                    'representationName' => 'Carte',
                    'created_at' => '2019-11-27 15:38:05',
                    'updated_at' => '2019-11-27 15:38:05',
                ),
            22 =>
                array(
                    'datasetId' => 7,
                    'representationName' => 'Graphique en colonnes',
                    'created_at' => '2019-11-27 15:38:05',
                    'updated_at' => '2019-11-27 15:38:05',
                ),
            23 =>
                array(
                    'datasetId' => 7,
                    'representationName' => 'Graphique en secteur',
                    'created_at' => '2019-11-27 15:38:05',
                    'updated_at' => '2019-11-27 15:38:05',
                ),
            24 =>
                array(
                    'datasetId' => 7,
                    'representationName' => 'Somme éléments',
                    'created_at' => '2019-11-27 15:38:05',
                    'updated_at' => '2019-11-27 15:38:05',
                ),
            25 =>
                array(
                    'datasetId' => 7,
                    'representationName' => 'Tableau',
                    'created_at' => '2019-11-27 15:38:05',
                    'updated_at' => '2019-11-27 15:38:05',
                ),
            26 =>
                array(
                    'datasetId' => 8,
                    'representationName' => 'Carte',
                    'created_at' => '2019-11-27 15:23:49',
                    'updated_at' => '2019-11-27 15:23:49',
                ),
            27 =>
                array(
                    'datasetId' => 8,
                    'representationName' => 'Graphique en colonnes',
                    'created_at' => '2019-11-27 15:23:49',
                    'updated_at' => '2019-11-27 15:23:49',
                ),
            28 =>
                array(
                    'datasetId' => 8,
                    'representationName' => 'Graphique en secteur',
                    'created_at' => '2019-11-27 15:23:49',
                    'updated_at' => '2019-11-27 15:23:49',
                ),
            29 =>
                array(
                    'datasetId' => 8,
                    'representationName' => 'Somme éléments',
                    'created_at' => '2019-11-27 15:23:49',
                    'updated_at' => '2019-11-27 15:23:49',
                ),
            30 =>
                array(
                    'datasetId' => 8,
                    'representationName' => 'Tableau',
                    'created_at' => '2019-11-27 15:23:49',
                    'updated_at' => '2019-11-27 15:23:49',
                ),
            31 =>
                array(
                    'datasetId' => 9,
                    'representationName' => 'Carte',
                    'created_at' => '2019-12-02 13:09:24',
                    'updated_at' => '2019-12-02 13:09:24',
                ),
            32 =>
                array(
                    'datasetId' => 9,
                    'representationName' => 'Graphique en colonnes',
                    'created_at' => '2019-12-02 13:09:24',
                    'updated_at' => '2019-12-02 13:09:24',
                ),
            33 =>
                array(
                    'datasetId' => 9,
                    'representationName' => 'Graphique en courbes',
                    'created_at' => '2019-12-02 13:09:24',
                    'updated_at' => '2019-12-02 13:09:24',
                ),
            34 =>
                array(
                    'datasetId' => 9,
                    'representationName' => 'Graphique en secteur',
                    'created_at' => '2019-12-02 13:09:24',
                    'updated_at' => '2019-12-02 13:09:24',
                ),
            35 =>
                array(
                    'datasetId' => 9,
                    'representationName' => 'Somme éléments',
                    'created_at' => '2019-12-02 13:09:24',
                    'updated_at' => '2019-12-02 13:09:24',
                ),
            36 =>
                array(
                    'datasetId' => 9,
                    'representationName' => 'Tableau',
                    'created_at' => '2019-12-02 13:09:24',
                    'updated_at' => '2019-12-02 13:09:24',
                ),
            37 =>
                array(
                    'datasetId' => 10,
                    'representationName' => 'Carte',
                    'created_at' => '2019-12-03 09:34:05',
                    'updated_at' => '2019-12-03 09:34:05',
                ),
            38 =>
                array(
                    'datasetId' => 10,
                    'representationName' => 'Tableau',
                    'created_at' => '2019-12-03 09:34:05',
                    'updated_at' => '2019-12-03 09:34:05',
                ),
            39 =>
                array(
                    'datasetId' => 11,
                    'representationName' => 'Carte',
                    'created_at' => '2019-12-03 09:35:21',
                    'updated_at' => '2019-12-03 09:35:21',
                ),
            40 =>
                array(
                    'datasetId' => 11,
                    'representationName' => 'Graphique en colonnes',
                    'created_at' => '2019-12-03 09:35:21',
                    'updated_at' => '2019-12-03 09:35:21',
                ),
            41 =>
                array(
                    'datasetId' => 11,
                    'representationName' => 'Graphique en courbes',
                    'created_at' => '2019-12-03 15:29:17',
                    'updated_at' => '2019-12-03 15:29:17',
                ),
            42 =>
                array(
                    'datasetId' => 11,
                    'representationName' => 'Graphique en radar',
                    'created_at' => '2019-12-03 09:35:21',
                    'updated_at' => '2019-12-03 09:35:21',
                ),
            43 =>
                array(
                    'datasetId' => 11,
                    'representationName' => 'Graphique en secteur',
                    'created_at' => '2019-12-03 10:11:16',
                    'updated_at' => '2019-12-03 10:11:16',
                ),
            44 =>
                array(
                    'datasetId' => 11,
                    'representationName' => 'Tableau',
                    'created_at' => '2019-12-03 09:35:21',
                    'updated_at' => '2019-12-03 09:35:21',
                ),
            45 =>
                array(
                    'datasetId' => 12,
                    'representationName' => 'Carte',
                    'created_at' => '2019-12-02 15:15:26',
                    'updated_at' => '2019-12-02 15:15:26',
                ),
            46 =>
                array(
                    'datasetId' => 12,
                    'representationName' => 'Graphique en anneau',
                    'created_at' => '2019-12-02 15:15:26',
                    'updated_at' => '2019-12-02 15:15:26',
                ),
            47 =>
                array(
                    'datasetId' => 12,
                    'representationName' => 'Graphique en barres',
                    'created_at' => '2019-12-02 15:15:27',
                    'updated_at' => '2019-12-02 15:15:27',
                ),
            48 =>
                array(
                    'datasetId' => 12,
                    'representationName' => 'Graphique en colonnes',
                    'created_at' => '2019-12-02 15:15:27',
                    'updated_at' => '2019-12-02 15:15:27',
                ),
            49 =>
                array(
                    'datasetId' => 12,
                    'representationName' => 'Graphique en courbes',
                    'created_at' => '2019-12-02 15:15:27',
                    'updated_at' => '2019-12-02 15:15:27',
                ),
            50 =>
                array(
                    'datasetId' => 12,
                    'representationName' => 'Graphique en radar',
                    'created_at' => '2019-12-02 15:15:27',
                    'updated_at' => '2019-12-02 15:15:27',
                ),
            51 =>
                array(
                    'datasetId' => 12,
                    'representationName' => 'Graphique en secteur',
                    'created_at' => '2019-12-02 15:15:27',
                    'updated_at' => '2019-12-02 15:15:27',
                ),
            52 =>
                array(
                    'datasetId' => 12,
                    'representationName' => 'Graphique polaire',
                    'created_at' => '2019-12-02 15:15:27',
                    'updated_at' => '2019-12-02 15:15:27',
                ),
            53 =>
                array(
                    'datasetId' => 12,
                    'representationName' => 'Somme éléments',
                    'created_at' => '2019-12-02 15:15:27',
                    'updated_at' => '2019-12-02 15:15:27',
                ),
            54 =>
                array(
                    'datasetId' => 12,
                    'representationName' => 'Tableau',
                    'created_at' => '2019-12-02 15:15:27',
                    'updated_at' => '2019-12-02 15:15:27',
                ),
            55 =>
                array(
                    'datasetId' => 14,
                    'representationName' => 'Graphique en courbes',
                    'created_at' => '2019-12-03 15:29:57',
                    'updated_at' => '2019-12-03 15:29:57',
                ),
            56 =>
                array(
                    'datasetId' => 14,
                    'representationName' => 'Graphique en radar',
                    'created_at' => '2019-12-03 15:29:57',
                    'updated_at' => '2019-12-03 15:29:57',
                ),
            57 =>
                array(
                    'datasetId' => 14,
                    'representationName' => 'Graphique en secteur',
                    'created_at' => '2019-12-03 15:29:57',
                    'updated_at' => '2019-12-03 15:29:57',
                ),
            58 =>
                array(
                    'datasetId' => 14,
                    'representationName' => 'Tableau',
                    'created_at' => '2019-12-03 15:29:57',
                    'updated_at' => '2019-12-03 15:29:57',
                ),
            59 =>
                array(
                    'datasetId' => 15,
                    'representationName' => 'Graphique en courbes',
                    'created_at' => '2019-12-03 15:32:09',
                    'updated_at' => '2019-12-03 15:32:09',
                ),
            60 =>
                array(
                    'datasetId' => 15,
                    'representationName' => 'Graphique en radar',
                    'created_at' => '2019-12-03 15:32:09',
                    'updated_at' => '2019-12-03 15:32:09',
                ),
            61 =>
                array(
                    'datasetId' => 15,
                    'representationName' => 'Graphique en secteur',
                    'created_at' => '2019-12-03 15:32:09',
                    'updated_at' => '2019-12-03 15:32:09',
                ),
            62 =>
                array(
                    'datasetId' => 15,
                    'representationName' => 'Tableau',
                    'created_at' => '2019-12-03 15:32:09',
                    'updated_at' => '2019-12-03 15:32:09',
                ),
            63 =>
                array(
                    'datasetId' => 16,
                    'representationName' => 'Graphique en courbes',
                    'created_at' => '2019-12-03 15:33:19',
                    'updated_at' => '2019-12-03 15:33:19',
                ),
            64 =>
                array(
                    'datasetId' => 16,
                    'representationName' => 'Graphique en radar',
                    'created_at' => '2019-12-03 15:33:19',
                    'updated_at' => '2019-12-03 15:33:19',
                ),
            65 =>
                array(
                    'datasetId' => 16,
                    'representationName' => 'Graphique en secteur',
                    'created_at' => '2019-12-03 15:33:19',
                    'updated_at' => '2019-12-03 15:33:19',
                ),
            66 =>
                array(
                    'datasetId' => 16,
                    'representationName' => 'Somme éléments',
                    'created_at' => '2019-12-03 15:33:19',
                    'updated_at' => '2019-12-03 15:33:19',
                ),
            67 =>
                array(
                    'datasetId' => 16,
                    'representationName' => 'Tableau',
                    'created_at' => '2019-12-03 15:33:19',
                    'updated_at' => '2019-12-03 15:33:19',
                ),
            68 =>
                array(
                    'datasetId' => 17,
                    'representationName' => 'Graphique en anneau',
                    'created_at' => '2019-12-03 17:19:56',
                    'updated_at' => '2019-12-03 17:19:56',
                ),
            69 =>
                array(
                    'datasetId' => 17,
                    'representationName' => 'Graphique en barres',
                    'created_at' => '2019-12-03 17:19:56',
                    'updated_at' => '2019-12-03 17:19:56',
                ),
            70 =>
                array(
                    'datasetId' => 17,
                    'representationName' => 'Graphique en colonnes',
                    'created_at' => '2019-12-03 15:31:00',
                    'updated_at' => '2019-12-03 15:31:00',
                ),
            71 =>
                array(
                    'datasetId' => 17,
                    'representationName' => 'Graphique en courbes',
                    'created_at' => '2019-12-03 15:31:00',
                    'updated_at' => '2019-12-03 15:31:00',
                ),
            72 =>
                array(
                    'datasetId' => 17,
                    'representationName' => 'Graphique en radar',
                    'created_at' => '2019-12-03 17:19:56',
                    'updated_at' => '2019-12-03 17:19:56',
                ),
            73 =>
                array(
                    'datasetId' => 17,
                    'representationName' => 'Graphique en secteur',
                    'created_at' => '2019-12-03 15:31:01',
                    'updated_at' => '2019-12-03 15:31:01',
                ),
            74 =>
                array(
                    'datasetId' => 17,
                    'representationName' => 'Graphique polaire',
                    'created_at' => '2019-12-03 17:19:56',
                    'updated_at' => '2019-12-03 17:19:56',
                ),
            75 =>
                array(
                    'datasetId' => 17,
                    'representationName' => 'Somme éléments',
                    'created_at' => '2019-12-03 17:19:56',
                    'updated_at' => '2019-12-03 17:19:56',
                ),
            76 =>
                array(
                    'datasetId' => 17,
                    'representationName' => 'Tableau',
                    'created_at' => '2019-12-03 15:31:01',
                    'updated_at' => '2019-12-03 15:31:01',
                ),
            77 =>
                array(
                    'datasetId' => 18,
                    'representationName' => 'Carte',
                    'created_at' => '2019-12-03 15:40:46',
                    'updated_at' => '2019-12-03 15:40:46',
                ),
            78 =>
                array(
                    'datasetId' => 18,
                    'representationName' => 'Graphique en courbes',
                    'created_at' => '2019-12-03 15:40:46',
                    'updated_at' => '2019-12-03 15:40:46',
                ),
            79 =>
                array(
                    'datasetId' => 18,
                    'representationName' => 'Graphique en secteur',
                    'created_at' => '2019-12-03 15:40:46',
                    'updated_at' => '2019-12-03 15:40:46',
                ),
            80 =>
                array(
                    'datasetId' => 18,
                    'representationName' => 'Tableau',
                    'created_at' => '2019-12-03 15:40:46',
                    'updated_at' => '2019-12-03 15:40:46',
                ),
        ));


    }
}
