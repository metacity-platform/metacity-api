<?php
/**
 * <API - Metacity>
 * Copyright (C) 2020.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Tests\Http\Services;

use App\color;
use App\direction;
use App\Http\Services\UserService;
use App\role;
use App\service;
use App\theme;
use App\user;
use App\user_theme;
use Exception as ExceptionAlias;
use Illuminate\Http\Request;
use Tests\TestCase;

class UserServiceTest extends TestCase
{

    public function testRemoveColorFromUserService()
    {
        $rand_color = "#000000";
        while (True) {
            $rand_color = '#' . dechex(mt_rand(0, 16777215));
            if (!color::where("color_code", $rand_color)->first()) {
                break;
            }
        }

        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();
        $request["color_code"] = $rand_color;

        $newColor = array(
            'user_id' => 1,
            'color_code' => $rand_color,
            'created_at' => NOW(),
            'updated_at' => NOW()
        );

        color::insert($newColor);
        $result = color::where("color_code", $rand_color)->first()["color_code"];
        $this->assertEquals($rand_color, $result);

        UserService::removeColorFromUserService($request);
        $result = color::where("color_code", $rand_color)->first();
        $this->assertEmpty($result);
    }

    public function testUpdateUserWithDataService()
    {
        $testUser = [
            "user_id" => 69420,
            "firstname" => "Php",
            "lastname" => "Unit",
            "role" => "test",
            "service" => "test",
            "direction" => "test",
            "mail" => "test@phpUnit.com",
            "password" => "password",
            "phone" => 010203040506];

        user::create($testUser);

        $testUser["role"] = role::get('role')->first()->role;
        $testUser["service"] = service::get('service')->first()->service;
        $testUser["direction"] = direction::get('direction')->first()->direction;
        $testUser["firstname"] = "LeTest";
        $testUser["lastname"] = "Oui";
        $testUser["mail"] = "laNewAdresse@mail.com";
        $testUser["phone"] = "0660504030";
        $testUser["password"] = "LeMdp0";

        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();
        $request["user_id"] = 69420;
        $request["role"] = $testUser["role"];
        $request["firstname"] = $testUser["firstname"];
        $request["lastname"] = $testUser["lastname"];
        $request["service"] = $testUser["service"];
        $request["direction"] = $testUser["direction"];
        $request["mail"] = $testUser["mail"];
        $request["phone"] = $testUser["phone"];
        $request["password"] = $testUser["password"];

        UserService::updateUserWithDataService($request);

        $user = user::get(
            ["user_id", "firstname", "lastname", "role", "service", "direction", "mail", "password", "phone"])->last();

        $this->assertTrue($user->password != "password");
        $testUser["password"] = $user->password;

        $this->assertEquals(json_encode($testUser), $user);

        user::where('user_id', 69420)->delete();
        $result = user::where('user_id', 69420)->first();
        $this->assertEmpty($result);
    }

    public function testDeleteUserThemeService()
    {
        $id = 69420;
        $name = theme::first()["name"];

        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();
        $request["user_id"] = $id;
        $request["name"] = $name;

        $user_theme = array(
            'user_id' => $id,
            'name' => $name,
            'created_at' => NOW(),
            'updated_at' => NOW()
        );

        user_theme::insert($user_theme);
        $result = user_theme::where("user_id", $id)->get();
        $this->assertNotEmpty($result);

        UserService::deleteUserThemeService($request);
        $result = user_theme::where("user_id", $id)->get();
        $this->assertEmpty($result);
    }

    public function testUpdateColorUserService()
    {
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();

        $colors = [];
        $i = 0;
        while ($i < 10) {
            $rand_color = '#' . dechex(mt_rand(0, 16777215));
            if (!color::where("color_code", $rand_color)->first()) {
                array_push($colors, $rand_color);
                $i++;
            }
        }

        $request["user"]->user_id = 69420;
        $request["colors"] = $colors;

        UserService::updateColorUserService($request);
        $result = color::whereIn("color_code", $colors)->get();
        $this->assertEquals(10, $result->count());

        $request["colors"] = [];
        UserService::updateColorUserService($request);
        $result = color::whereIn("color_code", $colors)->get();
        $this->assertEmpty($result);
    }

    public function testGetUsersNameService()
    {
        $result = UserService::getUsersNameService(null);
        $userName = user::all('user_id', 'firstname', 'lastname');

        $this->assertEquals($userName, $result);
    }

    public function testBlockUserService()
    {
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();

        UserService::blockUserService($request, 1);
        $result = user::where("user_id", 1)->first()["role"];
        $this->assertEquals("Désactivé", $result);

        User::where("user_id", 1)->update(["role" => "Administrateur"]);
        $result = user::where("user_id", 1)->first()["role"];
        $this->assertEquals("Administrateur", $result);

    }

    public function testGetAllUserColorService()
    {
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();

        $result = UserService::getAllUserColorService($request);
        $colors = color::where('user_id', 1)->get();
        $this->assertEquals($colors, $result);
    }

    public function testUnblockUserService()
    {
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();
        $request["user"]->role = "Administrateur";

        User::where("user_id", 1)->update(["role" => "Désactivé"]);
        $result = user::where("user_id", 1)->first()["role"];
        $this->assertEquals("Désactivé", $result);

        UserService::UnblockUserService($request, 1);
        $result = user::where("user_id", 1)->first()["role"];
        $this->assertEquals("Utilisateur", $result);

        User::where("user_id", 1)->update(["role" => "Administrateur"]);
        $result = user::where("user_id", 1)->first()["role"];
        $this->assertEquals("Administrateur", $result);
    }

    public function testAddUserThemeService()
    {
        $id = 1;
        $name = theme::first()["name"];

        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();
        $request["user_id"] = $id;
        $request["name"] = $name;

        UserService::addUserThemeService($request);
        $result = user_theme::where("user_id", $id)->where("name", $name)->first()["name"];
        $this->assertEquals($name, $result);

        user_theme::where("user_id", $id)->where("name", $name)->delete();
        $result = user_theme::where("user_id", $id)->where("name", $name)->get();
        $this->assertEmpty($result);
    }

    public function testAddColorToUserService()
    {
        $rand_color = "#000000";
        while (True) {
            $rand_color = '#' . dechex(mt_rand(0, 16777215));
            if (!color::where("color_code", $rand_color)->first()) {
                break;
            }
        }

        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();
        $request["user"]->user_id = 69420;
        $request["color_code"] = $rand_color;


        UserService::addColorToUserService($request);
        $result = color::where("color_code", $rand_color)->first()["color_code"];
        $this->assertEquals($rand_color, $result);

        color::where("color_code", $rand_color)->delete();
        $result = color::where("color_code", $rand_color)->first();
        $this->assertEmpty($result);
    }

    public function testAddUserService()
    {
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();

        $request["user_id"] = 69420;
        $request["role"] = "test";
        $request["firstname"] = "Php";
        $request["lastname"] = "Unit";
        $request["service"] = "test";
        $request["direction"] = "test";
        $request["mail"] = "test@phpUnit.com";
        $request["phone"] = 010203040506;
        $request["password"] = "password";

        $result = user::where('user_id', 69420)->first();
        $this->assertEmpty($result);

        userservice::addUserService(($request));

        $result = user::where('user_id', 69420)->first();
        $this->assertNotEmpty($result);

        user::where('user_id', 69420)->delete();

        $result = user::where('user_id', 69420)->first();
        $this->assertEmpty($result);
    }

    public function testGetAllUsersService()
    {
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();
        $user = user::get();

        $result = UserService::getAllUsersService($request);

        $this->assertEquals($user, $result);

        $request["user"]["role"] = "Guest";
        try {
            UserService::getAllUsersService($request);
            $this->assertTrue(false);
        } Catch (ExceptionAlias $e) {
            $this->assertContains("abort(", $e->getTraceAsString());
        }

    }

    public function testGetConnectedUserDataService()
    {
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();

        $result = userservice::getConnectedUserDataService($request);

        $this->assertNotEmpty($result);
    }
}
