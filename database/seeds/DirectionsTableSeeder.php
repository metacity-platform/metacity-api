<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DirectionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('directions')->delete();

        DB::table('directions')->insert(array(
            0 =>
                array(
                    'direction' => 'Mobilites Gestion Reseaux',
                    'description' => 'Direction Mobilites Gestion Reseaux',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            1 =>
                array(
                    'direction' => 'Numérique',
                    'description' => 'Direction du numérique',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
        ));


    }
}
