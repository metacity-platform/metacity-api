<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatasetHasTagsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('dataset_has_tags')->delete();


    }
}
