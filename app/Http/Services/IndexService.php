<?php

/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** @noinspection PhpUnused */
/** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpUnusedAliasInspection */

namespace App\Http\Services;


use App\analysis;
use App\analysis_column;
use App\caching;
use App\column;
use App\dataset;
use App\dataset_has_representation;
use App\dataset_has_tag;
use App\saved_card;
use App\user_saved_dataset;
use Illuminate\Http\Request;
use Elasticsearch;

class IndexService
{
    public static function getFieldsAndTypeService(Request $request, $name)
    {
        $checkRights = IndexService::checkRightsOnDataset($request, false, $name);
        $checkRightsValidate = IndexService::checkRightsOnDataset($request, true, $name);
        if (!($checkRights != false or $checkRightsValidate != false)) {
            $columns = null;
            abort(403);
        }

        $data = [
            'index' => $name
        ];
        $return = Elasticsearch::indices()->getMapping($data);
        $fields = [];
        foreach ($return[$name]['mappings']['properties'] as $field => $field_data) {
            if (gettype($field_data) == "array" && !array_key_exists('type', $field_data) && $field != "geometry") {
                foreach ($field_data['properties'] as $inner_field => $inner_field_data) {
                    if (!array_key_exists('type', $inner_field_data)) {
                        $fields[$field . $inner_field] = 'array';
                    } else {
                        $fields[$field . '.' . $inner_field] = $inner_field_data['type'];
                    }
                }
            } else if ($field != "geometry") {
                $fields[$field] = "array";
            } else {
                $fields[$field] = array_key_exists("properties", $field_data) ? $field_data['properties']["type"]["type"] : "array";
            }
        }
        return $fields;
    }

    public static function checkRightsOnDataset(Request $request, bool $validate, String $name = null)
    {
        if ($name == null) {
            $name = $request->get('name');
        }
        $datasets = AccessibleDatasetsService::getAllAccessibleDatasets($request, $request->get('user'), $validate);
        $canAccess = false;
        $datasetId = null;
        $dataset = null;

        foreach ($datasets as $data) {
            if ($name === $data->databaseName) {
                $datasetId = $data->id;
                $canAccess = true;
                break;
            }
        }
        if (!$canAccess) {
            return (false);
        }
        return $datasetId;
    }

    public static function getAllIndexService()
    {
        $stats = Elasticsearch::indices()->stats();
        return $stats['indices'];
    }

    public static function getAllDateFieldsFromAnIndexFromItsNameService(Request $request, $name)
    {
        $checkRights = IndexService::checkRightsOnDataset($request, false, $name);
        $checkRightsValidate = IndexService::checkRightsOnDataset($request, true, $name);
        if ($checkRights == false and $checkRightsValidate == false) {
            $columns = null;
            abort(403);
        }

        $data = [
            'index' => $name
        ];
        $return = Elasticsearch::indices()->getMapping($data);

        $date_fields = [];
        foreach ($return[$name]['mappings']['properties'] as $field => $field_data) {
            if (array_key_exists('type', $field_data) && $field_data['type'] == "date") {
                array_push($date_fields, $field);
            }
            if (gettype($field_data) == "array" && !array_key_exists('type', $field_data) && $field != "geometry") {
                foreach ($field_data['properties'] as $inner_field => $inner_field_data) {
                    if (array_key_exists('type', $field_data) && $field_data['type'] == "date") {
                        array_push($date_fields, $field);
                    }
                }
            }
        }

        return $date_fields;
    }

    public static function getAllFieldsFromIndexByNameService(Request $request, $name)
    {
        $user = $request->get('user');
        $canAccess = false;
        $datasets = AccessibleDatasetsService::getAllAccessibleDatasets($request, $user, true);
        foreach ($datasets as $dataset) {
            if ($name === $dataset->databaseName) {
                $canAccess = true;
            }
        }
        if (!$canAccess) {
            abort(403);
        }

        $data = [
            'index' => $name
        ];
        $return = Elasticsearch::indices()->getMapping($data);
        $count = Elasticsearch::search(['index' => $name, 'size' => 1, 'from' => 0]);
        $fields = [];
        foreach ($return[$name]['mappings']['properties'] as $field => $field_data) {
            if (gettype($field_data) == "array" && !array_key_exists('type', $field_data) && $field != "geometry") {
                foreach ($field_data['properties'] as $inner_field => $inner_field_data) {
                    array_push($fields, $field . "." . $inner_field);
                }
            } else {
                array_push($fields, $field);
            }
        }
        return ['count' => $count['hits']['total'], 'fields' => $fields];
    }

    public static function getAllAccessibleFieldsFromIndexByNameService(Request $request, $name)
    {
        $checkRights = IndexService::checkRightsOnDataset($request, false, $name);
        if ($checkRights == false) {
            $columns = null;
            abort(403);
        }


        $data = [
            'index' => $name
        ];
        $return = Elasticsearch::indices()->getMapping($data);
        $dataset = Dataset::where('databaseName', $name)->first();
        $accessibleFields = AccessibleDatasetsService::getAllAccessibleColumnsFromADatasetService($request, $dataset);
        $fields = [];
        foreach ($return[$name]['mappings']['properties'] as $field => $field_data) {
            if (gettype($field_data) == "array" && !array_key_exists('type', $field_data) && $field != "geometry") {
                foreach ($field_data['properties'] as $inner_field => $inner_field_data) {
                    if (!array_key_exists('type', $inner_field_data)) {
                        array_push($fields, [$field . $inner_field, 'array']);
                    } else {
                        array_push($fields, [$field . '.' . $inner_field, $inner_field_data['type']]);
                    }
                }
            } else if ($field != "geometry") {
                array_push($fields, [$field, $field_data['type']]);
            } else {
                array_push($fields, [$field, array_key_exists('properties', $field_data) ? $field_data['properties']["type"]["type"] : "array"]);
            }
        }


        $results = [];

        foreach ($accessibleFields as $acc_field) {
            foreach ($fields as $field) {
                if ($field[0] == $acc_field['name']) {
                    array_push($field, $acc_field['main']);
                    array_push($results, $field);
                }
            }
        }

        return $results;
    }

    public static function getIndexByNameService(Request $request, $name, $quantity = 5, $offset = 0, $date_col = null, $start_date = null, $end_date = null)
    {
        return IndexService::getIndexByNameQuantityAndOffsetService($request, $name, $quantity, $offset, $date_col, $start_date, $end_date);
    }

    public static function getIndexByNameQuantityAndOffsetService(Request $request, $name, $quantity = 5, $offset = 0, $date_col = null, $start_date = null, $end_date = null)
    {
        $user = $request->get('user');
        $datasets = AccessibleDatasetsService::getAllAccessibleDatasets($request, $user, false);
        $canAccess = false;
        $datasetId = null;
        foreach ($datasets as $dataset) {
            if ($name === $dataset->databaseName) {
                $datasetId = $dataset->id;
                $canAccess = true;
                break;
            }
        }

        $datasets = AccessibleDatasetsService::getAllAccessibleDatasets($request, $user, true);
        foreach ($datasets as $dataset) {
            if ($name === $dataset->databaseName) {
                $datasetId = $dataset->id;
                $canAccess = true;
                break;
            }
        }
        if (!$canAccess) {
            abort(403);
        }

        $columns = AccessibleDatasetsService::getAllAccessibleColumnsFromADatasetService($request, dataset::where('id', $datasetId)->first());
        $columnFilter = [];

        foreach ($columns as $column) {
            array_push($columnFilter, $column->name);
        }
        $body = [];
        if ($date_col != null && $start_date == null && $end_date == null) {
            $body = ['sort' => [[$date_col => ['order' => 'desc']]]];
        } elseif ($date_col != null && $start_date != null && $end_date == null) {
            $body = ['sort' => [$date_col => ['order' => 'desc']], 'query' => ['range' => [$date_col => ['gte' => $start_date, 'lte' => $start_date]]]];
        } elseif ($date_col != null && $start_date != null && $end_date != null) {
            $body = ['sort' => [$date_col => ['order' => 'desc']], 'query' => ['range' => [$date_col => ['gte' => $start_date, 'lte' => $end_date]]]];
        }

        return Elasticsearch::search(['index' => $name, '_source' => $columnFilter, 'track_total_hits' => true, 'size' => $quantity, "from" => $offset, "body" => $body]);
    }

    public static function getIndexFromCoordinatesInShapeService(Request $request)
    {
        $filter_dataset = $request->get('filter_dataset');
        $filter_field = $request->get('filter_field');

        $filter_id_field = $request->get('filter_id_field');
        $filter_id = $request->get('filter_id');

        $filtered_dataset = $request->get('filtered_dataset');
        $filtered_field = $request->get('filtered_field');

        //Fetch the geoShape data to be used as a filter
        $body = ['query' => ['match' => [$filter_id_field => $filter_id]]];
        $data = Elasticsearch::search(['index' => $filter_dataset, '_source' => [$filter_id_field, $filter_field], 'size' => 1, 'from' => 0, 'body' => $body]);
        $filter_data = $data['hits']['hits'][0]['_source'][$filter_field];

        $filter = ["query" => ["bool" => ["filter" => ["geo_shape" => [$filtered_field => ["shape" => $filter_data, "relation" => "within"]]]]]];

        return Elasticsearch::search(['index' => $filtered_dataset, 'size' => 1, 'from' => 0, 'body' => $filter]);
    }

    public static function getInPointInPolygonService(Request $request)
    {
        $checkRights = IndexService::checkRights($request, false);
        if ($checkRights == false) {
            $columns = null;
            abort(403);
        } else {
            $columns = $checkRights;
        }

        $nameFilter = $request->get('nameFilter');
        $dataFilters = Elasticsearch::search(['index' => $nameFilter, '_source' => $request->get('filterColumn'),
            'size' => $request->get('size'),
            "from" => $request->get('offset')])["hits"]["hits"];


        $keyId = 1;
        $result = [];
        $doStats = (bool)$request->get('stats')["do_stats"];

        $ElasticSearchService = new ElasticSearchService($request);
        $minuteQuery = $ElasticSearchService->getMinuteFilter();
        $fullDayQuery = $ElasticSearchService->getWeekdayFilter();
        $matchQuery = $ElasticSearchService->getMatchFilter();
        $sortQuery = $ElasticSearchService->getSortFilter();

        $dateFilter = $ElasticSearchService->getFilter([], $minuteQuery, $fullDayQuery, $matchQuery, $sortQuery);

        foreach ($dataFilters as $dataFilter) {
            $path = $dataFilter["_source"];
            foreach (explode(".", $request->get('targetColumn')) as $field) {
                $path = $path[$field];
            }
            $polygon = $path[0][0];
            $body = ["query" => ["bool" => ["must" => [["match_all" => (object)null]],
                "filter" => [["geo_polygon" => ["geometry.coordinates" => ["points" => $polygon]]]]]]];

            $body = array_merge_recursive($body, $dateFilter);

            $name = $request->get('name');
            $data = Elasticsearch::search(['index' => $name, '_source' => $columns,
                'size' => $request->get('size'),
                "from" => $request->get('offset'),
                "body" => $body]);


            if ($doStats) {
                foreach ($data["hits"]["hits"] as $element) {
                    $element = $element["_source"];
                    $element["KeyId"] = $keyId;
                    $element["geometry"]["coordinates"] = $polygon;
                    array_push($result, $element);
                }
                $keyId++;
            } else {
                $NewData = [];
                foreach ($data["hits"]["hits"] as $element) {
                    $element = $element["_source"];
                    array_push($NewData, $element);
                }
                array_push($result, $NewData);
            }
        }

        $doJoin = (bool)$request->get('join')["do_join"];
        if ($doJoin) {
            $subRequest = $request["join"]["request"];
            $subRequest["stats"]["do_stats"] = false;
            $subRequest["user"] = $request->get("user");
            $subRequest = new Request($subRequest);
            $subResult = IndexService::joinService($subRequest)->getOriginalContent();
            $result = IndexService::do_join(1, [$subResult, $result], $request["join"]["joining"])[1];
        }
        if ($doStats) {
            $columns = $request->get("stats")["columns"];
            $columns["pivot"] = "KeyId";
            $columns["isDate"] = false;
            $result = IndexColumnService::do_stats($columns, $result);
        }
        return response($result);
    }

    public static function checkRights(Request $request, bool $validate, String $name = null)
    {
        $dataset = IndexService::checkRightsOnDataset($request, $validate, $name);
        $columns = IndexService::checkRightsOnColumns($request, $name);
        if (!($dataset and $columns)) {
            return false;
        }
        return $columns;
    }

    public static function checkRightsOnColumns(Request $request, String $name = null)
    {
        if ($name == null) {
            $name = $request->get('name');
        }
        $datasets = dataset::where('databaseName', $name)->first();
        if ($datasets == null) {
            abort(404);
        }
        $AccessibleColumns = AccessibleDatasetsService::getAllAccessibleColumnsFromADatasetService($request, $datasets);
        $columns = [];
        $canAccess = false;
        if ($request->get('columns') != null) {
            foreach ($AccessibleColumns as $column) {
                if (in_array($column->name, $request->get('columns'))) {
                    array_push($columns, $column->name);
                    $canAccess = true;
                }
            }
        }
        if (!$canAccess) {
            return (false);
        }
        return $columns;
    }

    public static function joinService(Request $request)
    {
        $data = [];
        $datasets = $request["datasets"];

        $body = $request->getContent();
        unset($body->user);
        $body = preg_replace("#\n|\t|\r| |#", "", $body);
        $cachingService = new CachingService($body);
        $cashHaveResult = null;
        if ($cachingService->getCaching()) {
            $cachingService->checkAge();
            $cashHaveResult = $cachingService->getResult();
            if ($cashHaveResult != false) {
                $result = json_decode($cashHaveResult);
                if (key_exists("original", $result)) {
                    $result = $result->{"original"};
                } else {
                    $result = json_encode($result);
                }
                return $result;
            }
        }

        for ($i = 0; $i < sizeof($datasets); $i++) {
            $body = $datasets[$i];
            $body["user"] = $request["user"];
            $subRequest = new Request($body);
            $results = IndexService::liteIndexService($subRequest)->getOriginalContent()["hits"]["hits"];
            $temp = [];
            foreach ($results as $result) {
                array_push($temp, $result["_source"]);
            }
            array_push($data, $temp);
            if ($i >= 1) {
                $columns = $request["joining"][$i - 1];
                $data = IndexService::do_join($i, $data, $columns);
            }
        }
        $data = $data[sizeof($datasets) - 1];

        if (!$request["stats"]["do_stats"]) {
            $resultS = response($data, 200);
        } else {
            $resultS = IndexColumnService::do_stats($request["stats"]["columns"], $data);
        }

        if ($cashHaveResult === false) {
            $cachingService->setResult($resultS);
        }

        return $resultS;
    }

    public static function liteIndexService(Request $request)
    {
        $checkRights = IndexService::checkRights($request, false);
        if ($checkRights == false) {
            $columnFilter = null;
            abort(403);
        } else {
            $request["columns"] = $checkRights;
        }

        $name = $request->get("name");
        if ((bool)dataset::select('realtime')->where('databaseName', $name)->first()["realtime"]) {
            $data = IndexService::getLiteIndexInflux($request);
        } else {
            $data = IndexService::getLiteIndexElastic($request);
        }
        return response($data, 200);
    }

    private static function getLiteIndexInflux(Request $request)
    {
        $result = (new influxDBService)->doFullQuery($request);
        $hits = [];
        foreach ($result as $element) {
            $hit = ["_index" => $request["name"], "_source" => $element];
            array_push($hits, $hit);
        }
        $result = ["hits" => ["total" => sizeof($result), "hits" => $hits]];
        return $result;
    }

    private static function getLiteIndexElastic(Request $request)
    {

        $ElasticSearchService = new ElasticSearchService($request);
        $minuteQuery = $ElasticSearchService->getMinuteFilter();
        $fullDayQuery = $ElasticSearchService->getWeekdayFilter();
        $matchQuery = $ElasticSearchService->getMatchFilter();
        $sortQuery = $ElasticSearchService->getSortFilter();

        $body = $ElasticSearchService->getFilter([], $minuteQuery, $fullDayQuery, $matchQuery, $sortQuery);


        return Elasticsearch::search(['index' => $request->get("name"), '_source' => $request->get("columns"),
            'size' => $request->get('size'),
            "from" => $request->get('offset'),
            "body" => $body]);
    }

    private static function do_join(int $i, array $data, array $columns)
    {
        $newData = [];
        foreach ($data[$i - 1] as $entry) {
            $path = $entry;
            foreach (explode(".", $columns[0]) as $field) {
                $path = $path[$field];
            }
            foreach ($data[$i] as $newEntry) {
                $newPath = $newEntry;
                foreach (explode(".", $columns[1]) as $field) {
                    $newPath = $newPath[$field];
                }
                if ($path == $newPath) {
                    array_push($newData, array_replace_recursive($entry, $newEntry));
                }
            }
        }
        array_unique($newData, SORT_REGULAR);
        $data[$i] = $newData;
        return $data;
    }

    public static function getLast(Request $request)
    {
        if (IndexService::checkRights($request, false) == false) {
            abort(403);
        }

        $client = (new InfluxDBService)->getClient();

        $select = 'last("' . implode('"), last("', $request["columns"]) . '")';
        $from = $request["name"];
        $groupBy = '"' . implode('", "', explode("+", $request["groupby"])) . '"';

        /** @noinspection PhpUnhandledExceptionInspection */
        $result = $client->query(env("INFLUXDB_DBNAME"), 'SELECT ' . $select . ' FROM ' . $from . ' GROUP BY ' . $groupBy)->getPoints();

        return response($result, 200);
    }

    public static function deleteIndexService(Request $request, $id)
    {
        if ($request["user"]["role"] != "Administrateur") {
            abort(403);
        }

        $name = dataset::select("databaseName")->where("id", $id)->get();

        if ($name != "[]") {
            $name = $name[0]["databaseName"];
        } else {
            abort(404);
        }

        dataset::where("id", $id)->delete();
        column::where('dataset_id', $id)->delete();
        user_saved_dataset::where("id", $id)->delete();

        dataset_has_representation::where("datasetId", $id)->delete();
        dataset_has_tag::where("id", $id)->delete();

        $analysis = analysis::select("id", "body")->get();
        $analysis_to_delete = [];
        foreach ($analysis as $analyse) {
            foreach (json_decode($analyse["body"], true)["datasets"] as $dataset) {
                if ($dataset["name"] == $name) {
                    array_push($analysis_to_delete, $analyse["id"]);
                }
            }
        }

        analysis::whereIn("id", $analysis_to_delete)->delete();
        analysis_column::whereIn("analysis_id", $analysis_to_delete)->delete();

        saved_card::whereIn("id", $analysis_to_delete)->delete();
        user_saved_dataset::whereIn("id", $analysis_to_delete)->delete();


        $caches = caching::select("id", "body")->get();
        $caches_to_delete = [];
        foreach ($caches as $cache) {
            foreach (json_decode($cache["body"], true)["datasets"] as $dataset) {
                if ($dataset["name"] == $name) {
                    array_push($caches_to_delete, $cache["id"]);
                }
            }
        }

        caching::whereIn("id", $caches_to_delete)->delete();

        if (Elasticsearch::indices()->exists(['index' => $name])) {
            Elasticsearch::indices()->delete(['index' => $name]);
        }
    }

    private static function countElementInIndexElastic($name)
    {
        return Elasticsearch::count(['index' => $name])["count"];
    }

    public static function countElementInIndexService($name)
    {
        if (!(bool)dataset::select('realtime')->where('databaseName', $name)->first()["realtime"]) {
            $data = IndexService::countElementInIndexElastic($name);
        } else {
            $data = null;
            abort(404);
        }
        return $data;
    }
}
