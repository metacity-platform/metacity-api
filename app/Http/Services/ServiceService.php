<?php

/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** @noinspection PhpUndefinedFieldInspection */

namespace App\Http\Services;


use App\service;
use Illuminate\Http\Request;

class ServiceService
{
    public static function getAllServicesServices()
    {
        return service::get();
    }

    public static function addServiceServices(Request $request)
    {
        $role = $request->get('user')->role;
        if ($role != "Administrateur") {
            abort(403);
        }
        $name = $request->get('service');
        $desc = $request->get('desc');
        $service = new service();
        $service->service = $name;
        $service->description = $desc;
        $service->save();
    }

    public static function delServiceServices(Request $request, $name)
    {
        $role = $request->get('user')->role;
        if ($role != "Administrateur") {
            abort(403);
        }
        $service = service::where('service', $name)->first();
        if ($service == null) {
            abort(403);
        }
        $service->delete();
    }

    public static function updateServiceServices(Request $request)
    {
        $role = $request->get('user')->role;
        if ($role != "Administrateur") {
            abort(403);
        }
        $name = $request->get('service');
        $newName = $request->get('newName');
        $desc = $request->get('desc');
        $service = service::where('service', $name)->first();
        if ($service == null) {
            abort(403);
        }
        $service->service = $newName != null ? $newName : $name;
        $service->description = $desc != null ? $desc : $service->description;
        $service->save();
    }
}
