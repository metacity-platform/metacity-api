<?php
/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** @noinspection PhpUndefinedFieldInspection */

namespace App\Http\Services;


use App\analysis;
use App\analysis_column;
use App\representation_type;
use App\saved_card;
use App\theme;
use App\user_theme;
use Exception;
use Illuminate\Http\Request;


class AnalyseService
{
    public static function saveAnalyseService(Request $request)
    {
        $user = $request->get('user');
        $analyse = new analysis();
        $analyse->name = $request->get('name');
        $representation = representation_type::where('name', $request->get('representation_type'))->first();
        if ($representation == null) {
            error_log("missing representation");
            abort(400, "bad representation");
        }
        $analyse->representation_type = $request->get('representation_type');
        $analyse->shared = $request->get('shared');
        $analyse->visibility = $request->get('visibility') != null ? $request->get('visibility') : 'all';
        $analyse->isStats = $request->get('isStats');
        $analyse->isMap = $request->get('isMap');
        $analyse->owner_id = $user->user_id;
        $analyse->description = $request->get('description');
        $analyse->body = json_encode($request->get('body'));
        $analyse->usage = $request->get('usage');
        $theme_name = theme::where('name', $request->get('theme_name'))->first();
        if ($theme_name == null) {
            error_log("missing theme");
            abort(400, "missing theme or theme don't exist");
        }
        $analyse->theme_name = $request->get('theme_name');

        $analyse->save();

        $analyse = analysis::where('name', $request->get('name'))->first();
        $analysis_columns = $request->get('analysis_column');
        AnalyseService::createAnalysisColumnService($analysis_columns, $analyse->id);

        $saved_card = new saved_card();
        $saved_card->id = $analyse->id;
        $saved_card->user_id = $user->user_id;
        $saved_card->displayed = false;
        $saved_card->size = null;
        $saved_card->position = null;
        $saved_card->save();

        return $analyse;
    }

    public static function createAnalysisColumnService($analysis_columns, $id)
    {
        for ($i = 0; $i < count($analysis_columns); $i++) {
            $analysis_column = new analysis_column();
            $analysis_column->field = $analysis_columns[$i]['field'];
            $analysis_column->analysis_id = $id;
            $analysis_column->databaseName = $analysis_columns[$i]['databaseName'];
            error_log($analysis_columns[$i]['databaseName']);
            $analysis_column->color_code = $analysis_columns[$i]['color_code'] == null ? '' : $analysis_columns[$i]['color_code'];
            error_log($analysis_columns[$i]['usage']);
            $analysis_column->usage = $analysis_columns[$i]['usage'];
            try {
                $analysis_column->save();
            } catch (Exception $e) {
                error_log('error');
            }
        }
    }

    public static function deleteAnalysisService(Request $request, $id)
    {
        if (AnalyseService::getAnalysisByIdService($request, $id) == []) {
            abort(403);
        }

        analysis::where('id', $id)->delete();
        analysis_column::where('analysis_id', $id)->delete();
        saved_card::where('id', $id)->delete();

        return response("", 200);
    }

    public static function getAnalysisByIdService(Request $request, $id)
    {

        return AnalyseService::getAllAccessibleAnalysisService($request, false, $id);
    }

    public static function getAllAccessibleAnalysisService(Request $request, bool $saved = false, $id = null)
    {
        $user = $request->get('user');
        $analysis = analysis::with('analysis_columns')->where(function ($query) use ($user) {
            if ($user["role"] == "Administrateur") {
                error_log('admin');
                $query->get();
            } else {
                $query->where('owner_id', $user["user_id"])
                    ->orWhere(function ($query) use ($user) {
                        $query->where('shared', 1)
                            ->where(function ($query) use ($user) {
                                $query->where("visibility", "all");
                                $query->orWhere("visibility", "worker")
                                    ->whereIn("theme_name", ObjectListToArrayService::getObjectListToArray(user_theme::where("user_id", $user["user_id"])
                                        ->get("name")));
                                if ($user["role"] == "Référent-Métier") {
                                    $query->orWhere("visibility", "job_referent")
                                        ->whereIn("theme_name", ObjectListToArrayService::getObjectListToArray(user_theme::where("user_id", $user["user_id"])
                                            ->get("name")));
                                }
                            });
                    });
            }
        })->where(function ($query) use ($user, $saved, $id) {
            if ($saved) {
                $query->whereIn("id", ObjectListToArrayService::getObjectListToArray(saved_card::where("user_id", $user["user_id"])
                    ->get("id"), "id"));
            }
            if ($id != null) {
                $query->where("id", $id);
            }
        })->where(function ($query) use ($request) {
            if ($request["themeName"] != null) {
                $query->whereIn("theme_name", $request["themeName"]);
            }
            if (key_exists("geojson", $request->toArray())) {
                $query->where("isMap", (integer)$request["geojson"]);
            }
        })->get();

        $result = [];
        foreach ($analysis as $analyse) {
            $nb_columns = 0;
            $ok_columns = 0;
            foreach ($analyse["analysis_columns"] as $column) {
                $nb_columns += 1;
                $request["name"] = $column["databaseName"];
                $request["columns"] = [$column["field"]];
                if (IndexService::checkRightsOnColumns($request) == [$column["field"]]) {
                    $ok_columns += 1;
                }
            }
            if ($nb_columns === $ok_columns) {
                array_push($result, $analyse);
            }
        }
        return $result;
    }

    public static function getAllSavedAnalysisService(Request $request)
    {
        $user = $request->get('user');
        $analysis = AnalyseService::getAllAccessibleAnalysisService($request, true);
        $savedCards = saved_card::where("user_id", $user["user_id"])->whereIn("id", ObjectListToArrayService::getObjectListToArray($analysis, "id"))->get();

        $result = [];
        foreach ($savedCards as $savedCard) {
            $key = array_search($savedCard["id"], array_column($analysis, 'id'));
            $savedCard["analysis"] = $analysis[$key];
            array_push($result, $savedCard);
        }
        return $result;
    }
}

