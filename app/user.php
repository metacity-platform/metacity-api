<?php
/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace App;

use Illuminate\Foundation\Auth\User as Authenticable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Laravel\Passport\HasApiTokens;

/**
 * @property mixed user_id
 * @property mixed role
 * @property mixed firstname
 * @property mixed lastname
 * @property mixed service
 * @property mixed direction
 * @property mixed mail
 * @property mixed phone
 * @property mixed password
 */
class user extends Authenticable
{
    use HasApiTokens;
    protected $primaryKey = 'user_id';

    protected $fillable = [
        'user_id',
        'role',
        'firstname',
        'lastname',
        'service',
        'direction',
        'mail',
        'phone',
        'password'
    ];

    private $rules = array(
        'user_id' => 'required|min:1',
        'role' => 'required|min:3',
        'firstname' => 'required|min:1',
        'lastname' => 'required|min:1',
        'service' => 'required|min:3',
        'direction' => 'required|min:3',
        'mail' => 'required|email',
        'phone' => 'max:10',
        'password' => 'required|min:6'
    );

    public function validate($data)
    {
        // make a new validator object
        $v = Validator::make($data, $this->rules);
        // return the result
        if ($v->fails()) {
            error_log(implode($v->errors()->all()));
            abort(400, $v->errors()->all());
        }
    }

    public function themes()
    {
        return $this->belongsToMany('App\theme', 'user_theme', 'user_id', 'name');
    }

    public function roles()
    {
        return $this->hasOne('App\role', 'role', 'role');
    }

    public function service()
    {
        return $this->hasOne('App\service', 'service', 'service');
    }

    public function direction()
    {
        return $this->hasOne('App\direction', 'direction', 'direction');
    }

    public function analysis()
    {
        return $this->hasMany('App\analysis', 'owner_id', 'user_id');
    }

    public function datasets()
    {
        return $this->belongsToMany('App\dataset', 'auth_users', 'user_id', 'id');
    }

    public function saved_datasets()
    {
        return $this->belongsToMany('App\dataset', 'user_saved_dataset', 'user_id', 'id');
    }

    public function columns()
    {
        return $this->belongsToMany('App\column', 'colauth_users', 'user_id', 'id');
    }

    public function cards()
    {
        return $this->hasMany('App\saved_card', 'user_id', 'user_id');
    }

    public function colors()
    {
        return $this->hasMany('App\color', 'user_id', 'user_id');
    }

    public function saved_analysis()
    {
        return $this->belongsToMany('App\analysis', 'saved_card', 'user_id', 'user_id');
    }
}

