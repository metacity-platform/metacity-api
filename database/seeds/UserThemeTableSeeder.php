<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserThemeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('user_theme')->delete();

        DB::table('user_theme')->insert(array(
            0 =>
                array(
                    'user_id' => 1,
                    'name' => 'Mobilité',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            1 =>
                array(
                    'user_id' => 1,
                    'name' => 'Transport',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
        ));


    }
}
