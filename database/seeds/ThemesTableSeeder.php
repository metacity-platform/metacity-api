<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ThemesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('themes')->delete();

        DB::table('themes')->insert(array(
            0 =>
                array(
                    'name' => 'Default',
                    'description' => 'N/A',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            1 =>
                array(
                    'name' => 'Finance',
                    'description' => 'Metier de la finance',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            2 =>
                array(
                    'name' => 'Mobilité',
                    'description' => 'Metier de la mobilité',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            3 =>
                array(
                    'name' => 'Transport',
                    'description' => 'Metier de le transport',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            4 =>
                array(
                    'name' => 'Voirie',
                    'description' => 'Metier de la voirie',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
        ));


    }
}
