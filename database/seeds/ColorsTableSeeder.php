<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ColorsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('colors')->delete();

        DB::table('colors')->insert(array(
            0 =>
                array(
                    'color_code' => '#262424',
                    'user_id' => 1,
                    'created_at' => '2020-02-03 10:53:12',
                    'updated_at' => '2020-02-03 10:53:12',
                ),
            1 =>
                array(
                    'color_code' => '#26a69a',
                    'user_id' => 1,
                    'created_at' => '2020-02-03 10:53:12',
                    'updated_at' => '2020-02-03 10:53:12',
                ),
            2 =>
                array(
                    'color_code' => '#5c6bc0',
                    'user_id' => 1,
                    'created_at' => '2020-02-03 10:53:12',
                    'updated_at' => '2020-02-03 10:53:12',
                ),
            3 =>
                array(
                    'color_code' => '#66bae3',
                    'user_id' => 1,
                    'created_at' => '2020-02-03 10:53:12',
                    'updated_at' => '2020-02-03 10:53:12',
                ),
            4 =>
                array(
                    'color_code' => '#66bb6a',
                    'user_id' => 1,
                    'created_at' => '2020-02-03 10:53:12',
                    'updated_at' => '2020-02-03 10:53:12',
                ),
            5 =>
                array(
                    'color_code' => '#c62828',
                    'user_id' => 1,
                    'created_at' => '2020-02-03 10:53:12',
                    'updated_at' => '2020-02-03 10:53:12',
                ),
            6 =>
                array(
                    'color_code' => '#e0abd0',
                    'user_id' => 1,
                    'created_at' => '2020-02-03 10:53:12',
                    'updated_at' => '2020-02-03 10:53:12',
                ),
            7 =>
                array(
                    'color_code' => '#e5007d',
                    'user_id' => 1,
                    'created_at' => '2020-02-03 10:53:12',
                    'updated_at' => '2020-02-03 10:53:12',
                ),
            8 =>
                array(
                    'color_code' => '#e94c16',
                    'user_id' => 1,
                    'created_at' => '2020-02-03 10:53:12',
                    'updated_at' => '2020-02-03 10:53:12',
                ),
            9 =>
                array(
                    'color_code' => '#edda34',
                    'user_id' => 1,
                    'created_at' => '2020-02-03 10:53:12',
                    'updated_at' => '2020-02-03 10:53:12',
                ),
        ));


    }
}
