<?php
/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** @noinspection PhpUnused */

namespace App\Http\Controllers;

use App\Http\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function getAllUsers(Request $request)
    {
        $users = UserService::getAllUsersService($request);
        return response($users)->header('Content-Type', 'application/json')
            ->header('charset', 'utf-8');
    }

    public function getConnectedUserData(Request $request)
    {
        $user = UserService::getConnectedUserDataService($request);
        return response($user)->header('Content-Type', 'application/json')
            ->header('charset', 'utf-8');
    }

    public function getUsersName(Int $quantity = null)
    {
        $users = UserService::getUsersNameService($quantity);
        return response($users)->header('Content-Type', 'application/json')
            ->header('charset', 'utf-8');
    }

    public function updateUserWithData(Request $request)
    {
        UserService::updateUserWithDataService($request);
        return response("", 200);
    }

    public function addUser(Request $request)
    {
        UserService::addUserService($request);
        return response("", 200);
    }

    public function addUserTheme(Request $request)
    {
        UserService::addUserThemeService($request);
        return response('', 200);
    }

    public function deleteUserTheme(Request $request)
    {
        UserService::deleteUserThemeService($request);
        return response('', 200);
    }

    public function blockUser(Request $request, $user_id)
    {
        UserService::blockUserService($request, $user_id);
        return response("", 200);
    }

    public function unblockUser(Request $request, $user_id)
    {
        UserService::unblockUserService($request, $user_id);
        return response("", 200);
    }

    public function getAllUserColor(Request $request)
    {
        $userColors = UserService::getAllUserColorService($request);
        return response($userColors)->header('Content-Type', 'application/json')
            ->header('charset', 'utf-8');
    }

    public function addColorToUser(Request $request)
    {
        UserService::addColorToUserService($request);
        return response("", 200);
    }

    public function removeColorFromUser(Request $request)
    {
        UserService::removeColorFromUserService($request);
        return response("", 200);
    }

    public function updateColorUser(Request $request)
    {
        UserService::updateColorUserService($request);
        return response("", 200);
    }
}
