<?php
/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace App\Http\Middleware;

use App\user;
use Carbon\Carbon;
use Closure;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use function GuzzleHttp\json_decode;


class UserAuth
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        $token = $request->header('Authorization');
        if (!isset($token)) {
            abort(401);
        }

        if (!config('use_sso')) {
            if ($token == "Juiploetdjtozvelnjzkfpofn") {
                $user = user::where('user_id', "2be8c158-29a7-42b3-a9fb-de9ec266e196")->first();
                $request->merge(['user' => $user]);
                return $next($request);
            }
        }

        $user = user::where('token', $token, 'token_expirate' > Carbon::now())->first();
        if ($user == null) {
            $client = new Client();
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];

            $res = $client->get('https://authsso.extranet.toulouse.fr/cas/oidc/profile', ['headers' => $headers]);
            $data = json_decode($res->getBody(), true);
            $value = $data["sub"];
            if (!isset($value)) {
                abort(403, "SSO bad request");
            }

            $user = user::where('tid', $value)->first();
            if ($user == null) {
                abort(403, "User not found");
            }
            $user->token = $token;
            $user->token_expirate = Carbon::now()->addHours(8);
            $user->save();
            $request->merge(['user' => $user]);
            return $next($request);
        }
        if ($user->role == "Désactivé") {
            abort(403, "account deactivated");
        }
        $request->merge(['user' => $user]);
        return $next($request);
    }
}
