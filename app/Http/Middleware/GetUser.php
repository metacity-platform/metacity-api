<?php

namespace App\Http\Middleware;

use App\user;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GetUser
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = Auth::user()->only(['user_id'])['user_id'];
        $user = user::where('user_id', $id)->first();
        $request->merge(['user' => $user]);
        return $next($request);
    }
}
