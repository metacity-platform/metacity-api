<?php
/**
 * <API - Metacity>
 * Copyright (C) 2020.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Tests\Http\Services;

use App\column;
use App\dataset;
use App\Http\Services\AccessibleDatasetsService;
use App\user;
use Illuminate\Http\Request;
use Tests\TestCase;

class AccessibleDatasetsServiceTest extends TestCase
{

    public function testGetAllAccessibleDatasets()
    {
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();

        $result = AccessibleDatasetsService::getAllAccessibleDatasets($request);
        $this->assertNotEmpty($result);

        $result = AccessibleDatasetsService::getAllAccessibleDatasets($request, null, true, true);
        $this->assertEmpty($result);

        $result = AccessibleDatasetsService::getAllAccessibleDatasets($request, null, true, true, true);
        $this->assertEmpty($result);
    }

    public function testGetAllAccessibleColumnsFromADatasetService()
    {
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();
        $dataset = dataset::where('id', 1)->first();
        $columns = column::where('dataset_id', 1)->get();


        $result = AccessibleDatasetsService::getAllAccessibleColumnsFromADatasetService($request, $dataset);
        $this->assertEquals($columns, $result);
    }
}
