<?php
/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return response("", 200);
});


//Index routes : Elasticsearch
Route::middleware(['auth:api', 'getUser'])->group(function () {
    Route::get('/index/{quantity?}', 'IndexController@getAllIndex');
    Route::get('/index/date/{name}', 'IndexController@getAllDateFieldsFromAnIndexFromItsName');
    Route::get('/index/fields/{name}', 'IndexController@getAllFieldsFromIndexByName');
    Route::get('/index/accessiblefields/{name}', 'IndexController@getAllAccessibleFieldsFromIndexByName');
    Route::get('/index/get/{name}/{quantity?}/{offset?}/{date_col?}/{start_date?}/{end_date?}', 'IndexController@getIndexByName');
    Route::get('/index/count/{name}', 'IndexController@countElementInIndex');
//Route::post('/index/geo', 'IndexController@getIndexFromCoordinatesInShape');
    Route::post('/liteIndex', 'IndexController@getLiteIndex');
    Route::Delete('/index/delete/{id}', 'IndexController@deleteIndex');

//Index routes: Elasticsearch and InfluxDB
    Route::post('/index/join', 'IndexController@join');
    Route::post('/index/fromPolygon', 'IndexController@getInPointInPolygon');

//Index routes: InfluxDB
    Route::post('/index/last', 'IndexController@getLast');


//Analyse routes : Mysql
    Route::post('/analyse/save', 'AnalyseController@saveAnalyse');
    Route::get('/analyse/get/{id}', 'AnalyseController@getAnalysisById');
    Route::post('/analyse/all', 'AnalyseController@getAllAccessibleAnalysis');
    Route::get('/analyse/saved', 'AnalyseController@getAllSavedAnalysis');
    Route::delete('/analyse/{id}', 'AnalyseController@deleteAnalysis');
//Datasets routes : Mysql
    Route::get('/datasets/data/validate', 'DatasetController@getDatasetsToValidate');
    Route::delete('/datasets/data/unValidate/{id}', 'DatasetController@unValidate');
    Route::post('/datasets/all/', 'DatasetController@getAllDatasets');
    Route::get('/datasets/representations/{id}', 'DatasetController@getRepresentationsOfDataset');
    Route::get('/dataset/{id}', "DatasetController@getDatasetById");
    Route::post('/datasets/update', "DatasetController@updateDataset");
    Route::post('/datasets/upload', 'DatasetController@uploadDataset');
    Route::get('/dataset/{id}/columns', "DatasetController@getAllColumnFromDataset");
    Route::get('/dataset/{id}/save', "DatasetController@saveDataset");
    Route::get('/dataset/{id}/favorite', "DatasetController@favoriteDataset");
    Route::get('/dataset/{id}/unsave', "DatasetController@unsaveDataset");
    Route::get('/dataset/{id}/unfavorite', "DatasetController@unsaveDataset");
    Route::get('/datasets/favorite/', "DatasetController@getAllAccessibleFavoriteDatasets");
    Route::get('/datasets/filters', 'DatasetController@getFilterDatasets');
    Route::get('/datasets/saved/', "DatasetController@getAllAccessibleSavedDatasets");
    Route::get('/datasets/size/{type?}', "DatasetController@getDatasetsSize");

//Users routes : Mysql
    Route::get('/user', 'UserController@getAllUsers');
    Route::get('self', 'UserController@getConnectedUserData');
    Route::get('/users/name/{quantity?}', 'UserController@getUsersName');
    Route::post('/user/create', 'UserController@addUser');
    Route::post('/user/theme', 'UserController@addUserTheme');
    Route::delete('/user/theme', 'UserController@deleteUserTheme');
    Route::get('/user/block/{uuid}', 'UserController@blockUser');
    Route::get('/user/unblock/{uuid}', 'UserController@unblockUser');
    Route::get('/user/color', 'UserController@getAllUserColor');
    Route::post('user/color', 'UserController@addColorToUser');
    Route::post('color/update', 'UserController@updateColorUser');
    Route::delete('user/color', 'UserController@removeColorFromUser');

//Datatypes routes : Mysql
    Route::get('/datatypes/{quantity?}', 'DataTypesController@getAllDataTypes');

//Representation types routes : Mysql
    Route::get('/representationTypes/{quantity?}', 'RepresentationTypesController@getAllRepresentationTypes');

//Columns routes : Mysql
    Route::post('/column/create', 'ColumnController@createColumn');
    Route::post('/column/stats', 'ColumnController@getStats');

//themes routes : Mysql
    Route::get('/theme', 'ThemeController@getAllThemes');
    Route::post('/theme', 'ThemeController@addTheme');
    Route::delete('/theme/{name}/{newName}', 'ThemeController@deleteTheme');
    Route::put('/theme', 'ThemeController@updateTheme');

//Roles routes : Mysql
    Route::get('/role', 'RolesController@getAllRoles');

//Tags routes : Mysql
    Route::get('/tag/{quantity?}', 'TagsController@getAllTags');

//Directions routes : Mysql
    Route::post('/direction', 'DirectionController@addDirection');
    Route::delete('/direction/{name}', 'DirectionController@delDirection');
    Route::put('/direction', 'DirectionController@updateDirection');
    Route::get('/direction', 'DirectionController@getAllDirections');

//Services routes : Mysql
    Route::post('/service', 'ServiceController@addService');
    Route::delete('/service/{name}', 'ServiceController@delService');
    Route::put('/service', 'ServiceController@updateService');
    Route::get('/service', 'ServiceController@getAllServices');

//Saved cards routes : Mysql
    Route::get('/saved_card', 'SavedCardsController@getAllSavedCards');
    Route::post('/saved_card', 'SavedCardsController@saveCard');
    Route::put('/saved_card', 'SavedCardsController@updateCard');
    Route::delete('/saved_card/{id}', 'SavedCardsController@deleteCard');

//Routes de test
//Route::get('/user/add/{uuid}','UserController@createUserIfDontExist');
    Route::post('/user/update/', 'UserController@updateUserWithData');
});
Route::post('/login', 'Auth\LoginController@login');
Route::post('/register', 'Auth\RegisterController@register');

