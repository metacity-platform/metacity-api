<?php
/**
 * <API - Metacity>
 * Copyright (C) 2020.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Tests\Http\Services;

use App\analysis;
use App\analysis_column;
use App\Http\Services\SavedCardsService;
use App\saved_card;
use App\user;
use Illuminate\Http\Request;
use Tests\TestCase;

class SavedCardsServiceTest extends TestCase
{

    public function testUpdateCardService()
    {
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();
        $id = 69420;
        $request["id"] = $id;
        $request["size"] = 4;

        $new_analysis = analysis::where("id", 1)->first()->replicate();
        $new_analysis->id = $id;
        $new_analysis->name = "test saved card service";
        $new_analysis->save();


        $new_saved_cards = saved_card::first()->replicate();
        $new_saved_cards->id = $id;
        $new_saved_cards->position = saved_card::max("position") + 1;
        $new_saved_cards->save();


        $result = analysis::where("id", $id)->first()->toArray();
        $new_analysis = $new_analysis->toArray();
        $this->assertEquals(sort($new_analysis), sort($result));

        $result = saved_card::where("id", $id)->first();
        $this->assertNotNull(($result));


        $request["size"] = $new_saved_cards->size = 2;
        $request["displayed"] = $new_saved_cards->displayed = 1;
        $request["position"] = $new_saved_cards->position += 1;
        $request["id"] = $new_saved_cards->id = $id;


        SavedCardsService::updateCardService($request);

        $result = saved_card::where("id", $id)->first()->toArray();
        $new_card = $new_saved_cards->toArray();
        $this->assertEquals($new_card, $result);

        analysis::where("id", $id)->delete();
        $result = analysis::where("id", $id)->first();
        $this->assertNull($result);

        saved_card::where("id", $id)->delete();
        $result = saved_card::where("id", $id)->first();
        $this->assertNull($result);
    }

    public function testDeleteCardService()
    {
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();
        $id = 69420;
        $request["id"] = $id;
        $request["size"] = 4;

        $new_analysis = analysis::where("id", 1)->first()->replicate();
        $new_analysis->id = $id;
        $new_analysis->name = "test saved card service";
        $new_analysis->save();


        $new_saved_cards = saved_card::first()->replicate();
        $new_saved_cards->id = $id;
        $new_saved_cards->position = saved_card::max("position") + 1;
        $new_saved_cards->save();


        $result = (array)analysis::where("id", $id)->first();
        $new_analysis = (array)$new_analysis;
        $this->assertEquals(sort($new_analysis), sort($result));

        $result = saved_card::where("id", $id)->first();
        $this->assertNotNull(($result));


        analysis::where("id", $id)->delete();
        $result = analysis::where("id", $id)->first();
        $this->assertNull($result);

        SavedCardsService::deleteCardService($request, $id);
        $result = saved_card::where("id", $id)->first();
        $this->assertNull($result);
    }

    public function testGetAllSavedCardsService()
    {
        $request = new Request();
        $user = user::where('user_id', 1)->first();
        $request["user"] = $user;

        $result = SavedCardsService::getAllSavedCardsService($request);

        $saved_cards = saved_card::where('user_id', $user->user_id)->get();
        foreach ($saved_cards as $saved_card) {
            $analysis = analysis::where('id', $saved_card->id)->first();
            $analysis->analysis_column = analysis_column::where('analysis_id', $saved_card->id)->get();
            $saved_card->analysis = $analysis;
        }

        $this->assertEquals($saved_cards, $result);
    }

    public function testSaveCardService()
    {
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();
        $id = 69420;
        $request["id"] = $id;
        $request["size"] = 4;

        $new_analysis = analysis::where("id", 1)->first()->replicate();
        $new_analysis->id = $id;
        $new_analysis->name = "test saved card service";
        $new_analysis->save();


        SavedCardsService::saveCardService($request);


        $result = analysis::where("id", $id)->first()->toArray();
        $new_analysis = $new_analysis->toArray();

        $this->assertEquals($new_analysis, $result);
        $result = saved_card::where("id", $id)->first();
        $this->assertNotNull(($result));


        analysis::where("id", $id)->delete();
        $result = analysis::where("id", $id)->first();
        $this->assertNull($result);

        saved_card::where("id", $id)->delete();
        $result = saved_card::where("id", $id)->first();
        $this->assertNull($result);
    }
}
