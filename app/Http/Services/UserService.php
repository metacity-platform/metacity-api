<?php

/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace App\Http\Services;

use App\color;
use App\direction;
use App\role;
use App\service;
use App\theme;
use App\user;
use App\user_theme;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserService
{
    public static function getAllUsersService(Request $request)
    {
        $role = $request->get('user')->role;
        if ($role != "Administrateur") {
            abort(403);
        }
        return user::all();
    }

    public static function getConnectedUserDataService(Request $request)
    {
        $user = $request->get('user');
        $themesData = user_theme::where('user_id', 1  /*$user['user_id']*/)->get('name');
        $themes = [];
        foreach ($themesData as $theme) {
            array_push($themes, (string)$theme["name"]);
        }
        $user["theme"] = $themes;
        return $user;
    }

    public static function getUsersNameService(Int $quantity = null)
    {
        if ($quantity == null) {
            $users = user::all('user_id', 'firstname', 'lastname');
        } else {
            $users = user::all('user_id', 'firstname', 'lastname')->take($quantity);
        }
        return $users;
    }

    public static function updateUserWithDataService(Request $request)
    {
        $role = $request->get('user')->role;
        if ($role != "Administrateur") {
            abort(403);
        }

        $user = user::where('user_id', '=', $request['user_id'])->first();
        if ($user == null) {
            abort(404);
        }

        $role = role::where('role', $request["role"])->first();
        if ($role == null) {
            abort(400, "Role does not exist ! ");
        }
        $user->role = $role->role;
        $user->firstname = $request["firstname"];
        $user->lastname = $request["lastname"];
        $service = service::where('service', $request["service"])->first();
        if ($service == null) {
            abort(400, "No service or does not exist");
        }
        $user->service = $request["service"];
        $direction = direction::where('direction', $request["direction"])->first();
        if ($direction == null) {
            abort(400, "No direction or does not exist");
        }
        $user->direction = $request["direction"];
        $user->mail = $request["mail"];
        $user->phone = $request["phone"];
        $user->user_id = $request["user_id"];
        $user->password = Hash::make($request['password']);
        $user->save();
    }

    public static function addUserService(Request $request)
    {

        $role = $request->get('user')->role;
        if ($role != "Administrateur") {
            abort(403);
        }

        $user = new user();
        $postBody = $request->all();
        $user->validate($postBody);

        $user->user_id = $request->get("user_id");
        $user->role = $request->get("role");
        $user->firstname = $request->get("firstname");
        $user->lastname = $request->get("lastname");
        $user->service = $request->get("service");
        $user->direction = $request->get("direction");
        $user->mail = $request->get("mail");
        $user->phone = $request->get("phone");
        $user->password = Hash::make($request['password']);
        $user->save();
    }

    public static function addUserThemeService(Request $request)
    {
        $role = $request->get('user')->role;
        if ($role != "Administrateur") {
            abort(403);
        }

        $userTheme = new user_theme();
        $postBody = $request->all();
        if (!$userTheme->validate($postBody)) {
            abort(400);
        }


        if (user::where('user_id', '=', $postBody["user_id"])->get("user_id") == '[]' or theme::where('name', '=', $postBody["name"])->get("name") == '[]') {
            abort(404);
        }

        try {
            $userTheme->user_id = $postBody['user_id'];
            $userTheme->name = $postBody['name'];
            $userTheme->save();
        } catch (Exception $e) {
            if (!($e->getCode() === 0)) {
                abort(400);
            }
        }
    }

    public static function deleteUserThemeService(Request $request)
    {
        $role = $request->get('user')->role;
        if ($role != "Administrateur") {
            abort(403);
        }

        if (user_theme::where('name', '=', $request->get('name'))
                ->where('user_id', '=', $request->get('user_id'))->get() == '[]') {
            abort(400);
        }
        user_theme::where('name', '=', $request->get('name'))
            ->where('user_id', '=', $request->get('user_id'))->delete();
    }

    public static function blockUserService(Request $request, $user_id)
    {
        $role = $request->get('user')->role;
        if ($role != "Administrateur") {
            abort(403);
        }

        $user = user::where('user_id', $user_id)->first();
        if ($user == null) {
            abort(404);
        }
        $user->role = "Désactivé";
        $user->save();
    }

    public static function unblockUserService(Request $request, $user_id)
    {
        $role = $request->get('user')->role;
        if ($role != "Administrateur") {
            abort(403);
        }

        $user = user::where('user_id', $user_id)->first();
        if ($user == null) {
            abort(404);
        }
        $user->role = "Utilisateur";
        $user->save();
    }

    public static function getAllUserColorService(Request $request)
    {
        $user = $request->get('user');
        return $user->colors;
    }

    public static function addColorToUserService(Request $request)
    {
        $user = $request->get('user');
        $color = $request->get('color_code');

        $data = array(
            'user_id' => $user->user_id,
            'color_code' => $color,
            'created_at' => NOW(),
            'updated_at' => NOW()
        );
        color::insert($data);
    }

    public static function removeColorFromUserService(Request $request)
    {
        $user = $request->get('user');
        $color = color::where([['user_id', $user->user_id], ['color_code', $request->get('color_code')]]);
        $color->delete();
    }

    public static function updateColorUserService(Request $request)
    {
        $user = $request->get('user');
        $colors = $request->get('colors');

        color::where('user_id', $user->user_id)->delete();

        $data = array();
        foreach ($colors as $color) {
            array_push($data, array(
                'user_id' => $user->user_id,
                'color_code' => $color,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ));
        }
        color::insert($data);
    }
}
