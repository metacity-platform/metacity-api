<?php
/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(RoleSeeder::class);
//        $this->call(DatasetSeeder::class);
//        $this->call(UserSeeder::class);
//        $this->call(DataTypeSeeder::class);
//        $this->call(RepresentationTypesSeeder::class);
//        $this->call(ThemeSeeder::class);
//        $this->call(user_theme_seeder::class);
//        $this->call(direction_seeder::class);
//        $this->call(service_seeder::class);
//        $this->call(tag_seeder::class);
        //$this->call(Auth_seeder::class);
        //$this->call(DatasetHasRepresentationSeeder::class);
        $this->call(AnalysesTableSeeder::class);
        $this->call(AnalysisColumnsTableSeeder::class);
        $this->call(CachingsTableSeeder::class);
        $this->call(ColorsTableSeeder::class);
        $this->call(ColumnsTableSeeder::class);
        $this->call(DataTypesTableSeeder::class);
        $this->call(DatasetHasRepresentationsTableSeeder::class);
        $this->call(DatasetHasTagsTableSeeder::class);
        $this->call(DatasetsTableSeeder::class);
        $this->call(DirectionsTableSeeder::class);
        $this->call(NotificationsTableSeeder::class);
        $this->call(RepresentationTypesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(SavedCardsTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(ThemesTableSeeder::class);
        $this->call(UserSavedDatasetsTableSeeder::class);
        $this->call(UserThemeTableSeeder::class);
        $this->call(UsersTableSeeder::class);
    }
}
