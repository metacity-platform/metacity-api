<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DataTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('data_types')->delete();

        DB::table('data_types')->insert(array(
            0 =>
                array(
                    'name' => 'Chaîne de caractères',
                    'description' => 'Chaîne de caractères',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            1 =>
                array(
                    'name' => 'Date',
                    'description' => 'Date',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            2 =>
                array(
                    'name' => 'GeoPoint',
                    'description' => 'GeoPoint',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            3 =>
                array(
                    'name' => 'GeoShape',
                    'description' => 'GeoShape',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            4 =>
                array(
                    'name' => 'Nombre',
                    'description' => 'Nombre',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
        ));


    }
}
