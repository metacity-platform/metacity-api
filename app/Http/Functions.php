<?php
/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace App\Http;


class Functions
{
    static function parseIndexJson($data)
    {
        foreach ($data["hits"]["hits"] as $key => $array) {
            foreach ($data["hits"]["hits"][$key]["_source"] as $key2 => $array2) {
                $str_to_change = Functions::unicode2html($str_to_change);
                $str_to_change = str_replace('\\', '', $str_to_change);
                $str_to_change = str_replace("\\\"", '"', $str_to_change);
                $str_to_change = substr($str_to_change, 1);
                $str_to_change = substr_replace($str_to_change, "", -1);
                $str_to_change = html_entity_decode($str_to_change);
                $str_to_change = json_decode($str_to_change);
            }

        }
        return $data;
    }

    static function unicode2html($str)
    {
        // Set the locale to something that's UTF-8 capable
        setlocale(LC_ALL, 'en_US.UTF-8');
        // Convert the codepoints to entities
        $str = preg_replace("/u([0-9a-fA-F]{4})/", "&#x\\1;", $str);
        // Convert the entities to a UTF-8 string
        return iconv("UTF-8", "ISO-8859-1//TRANSLIT", $str);
    }
}
