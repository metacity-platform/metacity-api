<?php

/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** @noinspection PhpUndefinedFieldInspection */

namespace App\Http\Services;


use App\direction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DirectionService
{
    public static function getAllDirectionsService()
    {
        return direction::all();
    }

    public static function addDirectionService(Request $request)
    {
        $role = $request->get('user')->role;
        if ($role != "Administrateur") {
            abort(403);
        }
        $name = $request->get('direction');
        $desc = $request->get('desc');
        error_log($name);
        $direction = new direction();
        $direction->direction = $name;
        $direction->description = $desc;
        $direction->save();
    }

    public static function delDirectionService(Request $request, $name)
    {
        $role = $request->get('user')->role;
        if ($role != "Administrateur") {
            abort(403);
        }
        $direction = direction::where('direction', $name)->first();
        if ($direction == null) {
            abort(403);
        }
        $direction->delete();
    }

    public static function updateDirectionService(Request $request)
    {
        $role = $request->get('user')->role;
        if ($role != "Administrateur") {
            abort(403);
        }
        $name = $request->get('direction');
        $newName = $request->get('newName');
        $desc = $request->get('desc');
        $direction = direction::where('direction', $name)->first();
        if ($direction == null) {
            abort(403);
        }
        $direction->direction = $newName != null ? $newName : $name;
        $direction->description = $desc != null ? $desc : $direction->description;
        $direction->save();
    }
}
