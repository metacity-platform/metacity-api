<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSavedDatasetsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('user_saved_datasets')->delete();

        DB::table('user_saved_datasets')->insert(array(
            0 =>
                array(
                    'id' => 1,
                    'user_id' => 1,
                    'favorite' => 1,
                    'created_at' => '2020-01-03 14:53:53',
                    'updated_at' => '2020-01-03 14:53:53',
                ),
            1 =>
                array(
                    'id' => 3,
                    'user_id' => 1,
                    'favorite' => 1,
                    'created_at' => '2020-01-03 14:53:55',
                    'updated_at' => '2020-01-03 14:53:55',
                ),
        ));


    }
}
