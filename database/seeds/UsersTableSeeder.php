<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('users')->delete();

        DB::table('users')->insert(array(
            0 =>
                array(
                    'user_id' => 1,
                    'firstname' => 'Demo',
                    'lastname' => 'Polis',
                    'role' => 'Administrateur',
                    'service' => 'Numérique',
                    'direction' => 'Numérique',
                    'mail' => 'contact@metapolis.fr',
                    'password' => '$2y$10$PRCnWDlVMuSrb1b2wmlM9uTtCYVUSjWtlIX7e6pCnrrBg8zlWdfwm',
                    'phone' => NULL,
                    'token' => NULL,
                    'token_expirate' => NULL,
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2020-02-25 14:26:14',
                ),
            1 =>
                array(
                    'user_id' => 2,
                    'firstname' => 'Post',
                    'lastname' => 'Man',
                    'role' => 'Administrateur',
                    'service' => 'Tech',
                    'direction' => 'Team',
                    'mail' => 'postman@metapolis.fr',
                    'password' => '$2y$10$idVwFWoWYorN6eOAAjFS/uCrUnAkMKfsBVSQwviqGLK1QyUVqKa5u',
                    'phone' => NULL,
                    'token' => NULL,
                    'token_expirate' => NULL,
                    'created_at' => '2020-01-14 09:20:14',
                    'updated_at' => '2020-01-14 09:20:14',
                ),
        ));


    }
}
