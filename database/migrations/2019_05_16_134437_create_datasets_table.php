<?php
/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatasetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datasets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('databaseName', 48)->unique();
            $table->boolean('validated');
            $table->longText('description');
            $table->string('creator',45);
            $table->string('contributor',45);
            $table->string('license',45);
            $table->dateTime('created_date');
            $table->dateTime('updated_date');
            $table->boolean('realtime');
            $table->boolean('conf_ready')->default(false);
            $table->boolean('upload_ready')->default(false);
            $table->boolean('open_data')->default(false);
            $table->string('visibility');
            $table->string('user',45);
            $table->boolean('JSON')->default(false);
            $table->boolean('GEOJSON')->default(false);
            //$table->boolean('util')->default(false);
            $table->string('producer',45);
            $table->string('themeName');
            $table->string('update_frequency')->default("Jamais");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datasets');
    }
}
