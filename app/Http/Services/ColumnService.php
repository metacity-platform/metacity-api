<?php
/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpUndefinedFieldInspection */

namespace App\Http\Services;


use App\colauth_users;
use App\column;
use App\dataset;
use App\theme;
use App\user;
use Elasticsearch\ClientBuilder;
use Illuminate\Http\Request;
use /** @noinspection PhpUnusedAliasInspection */
    Elasticsearch;

class ColumnService
{
    public static function createColumnService(Request $request)
    {
        $client = ClientBuilder::create()->setHosts([env("ELASTICSEARCH_HOST") . ":" . env("ELASTICSEARCH_PORT")])->build();

        $role = $request->get('user')->role;
        if ($role != "Référent-Métier" && $role != "Administrateur") {
            abort(403);
        }
        $postBody = "";
        if (count($request->json()->all())) {
            $postBody = $request->json()->all();
        } else {
            error_log("no body in requests");
            abort(400);
        }

        foreach (array_keys($postBody) as $key) {
            $element = $postBody[$key];
            $dataset = dataset::where('id', '=', $element["datasetId"])->first();
            if ($dataset === null) {
                if ($key = "user") {
                    break;
                }
                error_log("no dataset with that id");
                abort(404);
            }

            if ($element["name"] == null || $element["datasetId"] == null) {
                error_log("missing name, datatype or datasetId");
                abort(400);
            }

            $check = column::where('dataset_id', '=', $element["datasetId"])->where('name', '=', $element['name'])->get();
            if (count($check) > 0) {
                error_log("column already exists");
                abort(409);
            }
            $column = new column();
            $column->name = $element["name"];
            $column->main = isset($element["main"]) ? $element['main'] : false;
            $column->visibility = $element["visibility"] == "" ? dataset::select('visibility')->where("id", $column->dataset_id)->first()['visibility'] : $element['visibility'];
            $column->dataset_id = $element["datasetId"];
            $theme = theme::where('name', $element["theme"])->first();
            if ($theme == null && ($element['theme'] != null || $element['theme'] != "")) {
                error_log($theme);
                error_log($element["theme"]);
                abort(400);
            } elseif ($element["theme"] == null) {
                $column->themeName = dataset::select('themeName')->where("id", $column->dataset_id)->first()['themeName'];
            } else {
                $column->themeName = $element["theme"];
            }
            if ($element["unit"] != null) {
                $column->unit = $element["unit"];
            }

            $column->save();
            if (isset($element['users'])) {
                $users = $element['users'];
                $column = column::where('name', $element["name"])->where('dataset_id', $element["datasetId"])->first();
                foreach ($users as $user_id) {
                    $auth_user = user::where('user_id', $user_id)->first();
                    if ($auth_user == null) {
                        continue;
                    }
                    $auth_users = new colauth_users();
                    $auth_users->id = $column->id;
                    $auth_users->user_id = $auth_user->user_id;
                    $auth_users->save();
                }
            }

            $fields = IndexService::getFieldsAndTypeService($request, $dataset->databaseName);

            if ((bool)$column->main and $fields[$column->name] == "text" and $column->name != "geometry") {
                $paramsSettings = ['index' => $dataset->databaseName,
                    'body' => ["index.blocks.read_only_allow_delete" => false]];

                $paramsMapping = ['index' => $dataset->databaseName, 'include_type_name' => true,
                    'body' => ['properties' => [$column->name => ['type' => 'text', 'fielddata' => true]]]];

                $client->indices()->putSettings($paramsSettings);
                $client->indices()->putMapping($paramsMapping);
            }
        }
    }

    public static function getStatsService(Request $request)
    {
        $checkRights = (new IndexService)->checkRights($request, false);
        if ($checkRights == false) {
            $columns = null;
            abort(403);
        } else {
            $request["columns"] = $checkRights;
        }

        $name = $request->get('name');
        if ((bool)dataset::select('realtime')->where('databaseName', $name)->first()["realtime"]) {
            $data = ColumnService::getStatsInflux($request);
        } else {
            $data = ColumnService::getStatsElastic($request);
        }
        return response($data, 200);
    }

    private static function getStatsInflux(Request $request)
    {
        $result = (new InfluxDBService)->doFullQuery($request);

        $column = ["pivot" => $request->get("groupby"), "isDate" => false, "data" => $request["columns"]];
        $stats = IndexColumnService::do_stats($column, $result);

        $hits = [];
        foreach (array_keys($stats) as $key) {
            $hit = $stats[$key]["stats"];
            $hit["key"] = $key;
            array_push($hits, $hit);
        }

        $result = ["hits" => ["total" => sizeof($result), "hits" => []], "aggregations" => ["codes" => ["buckets" => $hits]]];
        return $result;
    }

    private static function getStatsElastic(Request $request)
    {

        $ElasticSearchService = new ElasticSearchService($request);

        $minuteQuery = $ElasticSearchService->getMinuteFilter();
        $fullDayQuery = $ElasticSearchService->getWeekdayFilter();
        $matchQuery = $ElasticSearchService->getMatchFilter();
        $sortQuery = $ElasticSearchService->getSortFilter();

        $body = $ElasticSearchService->getFilter([], $minuteQuery, $fullDayQuery, $matchQuery, $sortQuery);


        $aggs = [];
        foreach ($request["columns"] as $column) {
            $aggs[$column] = ["stats" => ["field" => $column]];
        }

        $group_by_column = $request->get('groupby');
        if ($group_by_column) {
            $body["aggs"] = ["codes" => ["terms" => ["field" => $group_by_column, "size" => 10000], "aggs" => $aggs]];
        } else {
            $body["aggs"] = $aggs;
        }

        return Elasticsearch::search(['index' => $request["name"],
            'size' => 0,
            "body" => $body]);
    }
}
