<?php
/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class analysis extends Model
{
    public function representation()
    {
        return $this->belongsTo('App\representation_type', 'representation_type', 'name');
    }

    public function theme()
    {
        return $this->belongsTo('App\theme', 'theme_name', 'name');
    }

    public function owner()
    {
        return $this->belongsTo('App\user', 'owner_id', 'user_id');
    }

    public function analysis_columns()
    {
        return $this->hasMany('App\analysis_column', 'analysis_id', 'id');
    }
}
