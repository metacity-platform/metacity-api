<?php
/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace App\Http\Services;


use App\analysis;
use App\column;
use App\dataset;
use App\theme;
use App\user_theme;
use Illuminate\Http\Request;


class ThemeService
{
    public static function getAllThemesService()
    {
        return theme::get();
    }

    public static function addThemeService(Request $request)
    {
        $role = $request->get('user')->role;
        if ($role != "Administrateur") {
            abort(403);
        }

        $theme = new Theme();
        $postBody = $request->all();

        $theme->name = $postBody["name"];
        $theme->description = $postBody["description"];
        $theme->save();
    }

    public static function deleteThemeService(Request $request, $name, $newName)
    {
        $role = $request->get('user')->role;
        if ($role != "Administrateur") {
            abort(403, "Unauthorized");
        }

        if ($name == $newName) {
            abort(400, "Bad request");
        }

        if (theme::where('name', urldecode($name))->get() == '[]' or theme::where('name', urldecode($newName))->get() == '[]') {
            abort(400, "Bad request");
        }

        $theme = theme::where('name', '=', urldecode($name));
        $theme->delete();

        user_theme::where('name', '=', $name)->update(['name' => $newName]);
        dataset::where('themeName', '=', $name)->update(['themeName' => $newName]);
        column::where('themeName', '=', $name)->update(['themeName' => $newName]);
        analysis::where('theme_name', '=', $name)->update(['theme_name' => $newName]);
    }

    public static function updateThemeService(Request $request)
    {
        $role = $request->get('user')->role;
        if ($role != "Administrateur") {
            abort(403);
        }
        $name = $request->get('name');
        $newName = $request->get('newName');
        $desc = $request->get('desc');
        $theme = theme::where('name', $name)->first();
        if ($theme == null) {
            abort(403);
        }

        if ($newName != null) {
            user_theme::where('name', '=', $theme->name)->update(['name' => $newName]);
            dataset::where('themeName', '=', $theme->name)->update(['themeName' => $newName]);
            column::where('themeName', '=', $theme->name)->update(['themeName' => $newName]);
            analysis::where('theme_name', '=', $theme->name)->update(['theme_name' => $newName]);
            $theme->name = $newName;
        }
        if ($desc != null) {
            $theme->description = $desc;
        }
        $theme->save();
    }
}
