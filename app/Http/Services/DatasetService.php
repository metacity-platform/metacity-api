<?php

/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** @noinspection PhpUndefinedFieldInspection */

namespace App\Http\Services;


use App\auth_users;
use App\column;
use App\dataset;
use App\dataset_has_representation;
use App\dataset_has_tag;
use App\representation_type;
use App\tag;
use App\theme;
use App\user;
use App\user_saved_dataset;
use Elasticsearch\ClientBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use JsonMachine\JsonMachine;

class DatasetService
{
    public static function updateDatasetService(Request $request)
    {
        $role = $request->get('user')->role;
        if ($role != "Référent-Métier" && $role != "Administrateur") {
            abort(403);
        }

        $dataset = null;
        if ($request->get('id') != null) {
            $dataset = dataset::where('id', '=', $request->get('id'))->first();
        }
        if ($dataset == null) {
            error_log("no dataset with that id");
            abort(400);
        }

        $name = $request->get('name');
        $description = $request->get('description');
        $tags = $request->get('tag');
        $producer = $request->get('producer');
        $license = $request->get('license');
        $created_date = $request->get('created_date');
        $creator = $request->get('creator');
        $contributor = $request->get('contributor');
        $frequency = $request->get('frequency');


        $visibility = $request->get('visibility');
        $theme = $request->get('theme');


        $dataset->name = $name;
        $dataset->description = $description;

        $dataset->producer = $producer;
        $dataset->license = $license;
        $dataset->created_date = $created_date;
        $dataset->creator = $creator;
        $dataset->contributor = $contributor;
        $dataset->update_frequency = $frequency;

        $dataset->visibility = $visibility;
        $theme_from_base = theme::where('name', $theme)->first();
        if ($theme_from_base == null) {
            error_log($theme_from_base);
            abort(400);
        }


        $dataset->validated = true;
        $dataset->save();

        $dataset = dataset::where('id', $request->get('id'))->first();
        $tags = json_decode($tags);
        if ($tags != null) {
            error_log("tags not null");
            foreach ($tags as $tag) {
                error_log("tag array");
                $_tag = tag::where('name', $tag)->first();
                if ($_tag == null) {
                    error_log("Créer un nouveau tag");
                    $_tag = new tag();
                    $_tag->name = $tag;
                    $_tag->save();
                }
                error_log("Créer la relation entre " . $dataset->name . " et " . $_tag->name);
                if ((dataset_has_tag::where('id', $dataset->id)->where('name', $_tag->name)->first() == null)) {
                    $dataset_tag = new dataset_has_tag();
                    $dataset_tag->id = $dataset->id;
                    $dataset_tag->name = $_tag->name;
                    $dataset_tag->save();
                }
            }
        }
        error_log("first foreach passed");
        $visualisations = $request->get('visualisations');
        $visualisations = json_decode($visualisations);
        foreach ($visualisations as $visualisation) {
            $type = representation_type::where('name', $visualisation)->first();
            if ((dataset_has_representation::where('representationName', $type->name)->where('datasetId', $request->get('id'))->first()) == null) {
                $types = new dataset_has_representation();
                $types->datasetId = $request->get('id');
                $types->representationName = $type->name;
                $types->save();
            }
        }
        error_log("second foreach passed");
        $users = $request->get('users');
        $users = json_decode($users);
        foreach ($users as $user_id) {
            $auth_user = user::where('user_id', $user_id)->first();
            if ($auth_user == null || ((auth_users::where('user_id', $auth_user->user_id)->where('id', $request->get('id'))->first()) != null)) {
                continue;
            }
            $auth_users = new auth_users();
            $auth_users->id = $request->get('id');
            $auth_users->user_id = $auth_user->user_id;
            $auth_users->save();
        }
        error_log("last foreach passed");

        $client = ClientBuilder::create()->setHosts([env("ELASTICSEARCH_HOST") . ":" . env("ELASTICSEARCH_PORT")])->build();
        $paramsSettings = ['index' => $dataset->databaseName,
            'body' => ["index.max_result_window" => 5000000]];
        $client->indices()->putSettings($paramsSettings);

    }

    public static function uploadDatasetService(Request $request)
    {
        $description = $request->get('description');
        $name = $request->get('name');

        $metier = $request->get('metier');


        $creator = $request->get('creator');
        $contributor = $request->get('contributor');
        $dataset = new dataset();
        $dataset->name = $name;
        $dataset->validated = false;
        $dataset->description = $description;
        $dataset->creator = $creator;
        $dataset->contributor = $contributor;
        $dataset->license = "Fermée";
        $dataset->created_date = Carbon::now();
        $dataset->updated_date = Carbon::now();
        $dataset->realtime = false;
        $dataset->conf_ready = false;
        $dataset->upload_ready = false;
        $dataset->open_data = false;
        $dataset->visibility = "job_referent";
        $dataset->user = $creator;
        $dataset->producer = $creator;
        $dataset->themeName = $metier;
        $dataset->databaseName = str_replace("-", "_", Str::slug($name));
        $file = $request->file('uploadFile');
        $fields = self::getFieldInFile($file);
        $file->move(storage_path() . '/uploads', $dataset->databaseName . '.' . $file->getClientOriginalExtension());
        $theme = theme::where('name', $metier)->first();

        if ($theme == null) {
            error_log($theme);
            error_log($metier);
            abort(400);
        }
        $dataset->save();

        return $fields;
    }

    private static function getFieldInFile(Object $file)
    {
        $fields = [];
        switch ($file->getClientOriginalExtension()) {
            case "json":
                $stream = JsonMachine::fromFile($file);
                foreach ($stream as $data) {
                    $fields = array_keys($data);
                    break;
                }
                break;
            case "geojson":
                $stream = JsonMachine::fromFile($file);
                foreach ($stream as $name => $data) {
                    if ($name == "features") {
                        foreach ($data as $element) {
                            $fields = array_keys($element["properties"]);
                            break 2;
                        }
                    }
                }
                break;
            case "csv":
                $handle = fopen($file, "r");
                $raw_line = trim(fgets($handle));
                fclose($handle);
                $fields = explode(',', $raw_line);
                break;
        }
        return $fields;
    }

    public static function getDatasetsToValidateService(Request $request)
    {
        return AccessibleDatasetsService::getAllAccessibleDatasets($request, $request->get('user'), true);
    }

    public static function getFilterDatasetsService(Request $request)
    {
        $datasets = AccessibleDatasetsService::getAllAccessibleDatasets($request);

        $filter_datasets = [];
        foreach ($datasets as $dataset) {
            if ($dataset['update_frequency'] == 'Production unique' && $dataset['GEOJSON'] == '1') {
                array_push($filter_datasets, $dataset);
            }
        }

        return $filter_datasets;
    }

    public static function getRepresentationsOfDatasetService($id)
    {
        $dataset = dataset::where('id', $id)->first();
        if ($dataset == null) {
            abort(404);
        }
        return DB::table('dataset_has_representations')
            ->join('representation_types', 'dataset_has_representations.representationName', '=', 'representation_types.name')
            ->where('dataset_has_representations.datasetId', $id)->get();

    }

    public static function getAllColumnFromDatasetService(Request $request, $id)
    {
        $dataset = dataset::where('id', $id)->first();
        return DatasetService::getAllAccessibleColumnsFromADatasetService($request, $dataset);
    }

    public static function getAllAccessibleColumnsFromADatasetService(Request $request, Dataset $dataset)
    {
        return AccessibleDatasetsService::getAllAccessibleColumnsFromADatasetService($request, $dataset);
    }

    public static function saveDatasetService(Request $request, $id)
    {
        $dataset = dataset::where('id', $id)->first();
        $user = $request->get('user');
        DatasetService::saveAndFavoriteDatasetService($user, $dataset);
    }

    public static function saveAndFavoriteDatasetService(user $user, dataset $dataset, $favorite = false)
    {
        $saved_ds = user_saved_dataset::where('user_id', $user->user_id)->where('id', $dataset->id)->first();
        if ($saved_ds == null) {
            $saved_ds = new user_saved_dataset();
            $saved_ds->id = $dataset->id;
            $saved_ds->user_id = $user->user_id;
        }
        $saved_ds->favorite = $favorite;
        $saved_ds->save();
    }

    public static function favoriteDatasetService(Request $request, $id)
    {
        $dataset = dataset::where('id', $id)->first();
        $user = $request->get('user');
        DatasetService::saveAndFavoriteDatasetService($user, $dataset, true);
    }

    public static function unSaveDatasetService(Request $request, $id)
    {
        $dataset = dataset::where('id', $id)->first();
        $user = $request->get('user');
        $saved_ds = user_saved_dataset::where('user_id', $user->user_id)->where('id', $dataset->id)->first();
        if ($saved_ds != null) {
            $saved_ds->delete();
        }
    }

    public static function getDatasetByIdService(Request $request, $id)
    {
        return AccessibleDatasetsService::getAllAccessibleDatasets($request, $request->get('user'), false, false, false, $id);
    }

    public static function getDatasetsSizeService(Request $request, $type)
    {
        $case = [];
        switch ($type) {
            case "all":
                $case = DatasetService::getAllDatasetsService($request, true);
                break;
            case "saved":
                $case = DatasetService::getAllAccessibleSavedDatasetsService($request, true);
                break;
            case "favorite":
                $case = DatasetService::getAllAccessibleFavoriteDatasetsService($request, true);
                break;
            default:
                abort(400);
        }

        return $case;
    }

    public static function getAllDatasetsService(Request $request, bool $count = false)
    {
        return AccessibleDatasetsService::getAllAccessibleDatasets($request, $request->get('user'), false, false, false, null, $count, $request->get('offset'), $request->get('size'));
    }

    public static function getAllAccessibleSavedDatasetsService(Request $request, bool $count = false)
    {
        return AccessibleDatasetsService::getAllAccessibleDatasets($request, $request->get('user'), false, true, false, null, $count, $request->get('offset'), $request->get('size'));
    }

    public static function getAllAccessibleFavoriteDatasetsService(Request $request, bool $count = false)
    {
        return AccessibleDatasetsService::getAllAccessibleDatasets($request, $request->get('user'), false, true, true, null, $count, $request->get('offset'), $request->get('size'));
    }

    public static function unValidateService(Request $request, $id)
    {
        $databaseName = dataset::where('id', $id)->get("databaseName")[0]["databaseName"];
        if (IndexService::checkRightsOnDataset($request, false, $databaseName) == false) {
            abort(403);
        }

        dataset::where('id', $id)->update(["validated" => 0]);
        column::where('dataset_id', $id)->delete();
        dataset_has_tag::where('id', $id)->delete();
    }
}
