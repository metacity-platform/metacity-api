<?php /**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** @noinspection PhpUnused */

namespace App\Http\Controllers;


use App\representation_type;

class RepresentationTypesController extends Controller
{
    function getAllRepresentationTypes($quantity = 0)
    {
        if ($quantity == 0) {
            $types = representation_type::all();
        } else {
            $types = representation_type::all()->take($quantity);
        }
        return response($types)->header('Content-Type', 'application/json')
            ->header('charset', 'utf-8');
    }
}
