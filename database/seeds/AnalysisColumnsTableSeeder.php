<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnalysisColumnsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('analysis_columns')->delete();

        DB::table('analysis_columns')->insert(array(
            0 =>
                array(
                    'analysis_id' => 2,
                    'field' => 'age_moy',
                    'databaseName' => 'conseil_de_quartier',
                    'color_code' => '',
                    'usage' => '{"axis":"y"}',
                    'created_at' => '2019-11-27 14:22:59',
                    'updated_at' => '2019-11-27 14:22:59',
                ),
            1 =>
                array(
                    'analysis_id' => 5,
                    'field' => 'age_moy',
                    'databaseName' => 'conseil_de_quartier',
                    'color_code' => '#5c6bc0',
                    'usage' => '{"axis":"y"}',
                    'created_at' => '2019-12-03 15:49:06',
                    'updated_at' => '2019-12-03 15:49:06',
                ),
            2 =>
                array(
                    'analysis_id' => 4,
                    'field' => 'consommation',
                    'databaseName' => 'consommation_de_leclairage_publique',
                    'color_code' => '#5c6bc0',
                    'usage' => '{"axis":"y","statType":["avg"]}',
                    'created_at' => '2019-12-03 15:28:48',
                    'updated_at' => '2019-12-03 15:28:48',
                ),
            3 =>
                array(
                    'analysis_id' => 1,
                    'field' => 'geometry',
                    'databaseName' => 'quartiers',
                    'color_code' => '',
                    'usage' => '{"axis":"x"}',
                    'created_at' => '2019-11-26 16:51:24',
                    'updated_at' => '2019-11-26 16:51:24',
                ),
            4 =>
                array(
                    'analysis_id' => 2,
                    'field' => 'geometry',
                    'databaseName' => 'quartiers',
                    'color_code' => '',
                    'usage' => '{"axis":"x"}',
                    'created_at' => '2019-11-27 14:22:59',
                    'updated_at' => '2019-11-27 14:22:59',
                ),
            5 =>
                array(
                    'analysis_id' => 3,
                    'field' => 'geometry',
                    'databaseName' => 'quartiers',
                    'color_code' => '',
                    'usage' => '{"axis":"x"}',
                    'created_at' => '2019-12-03 15:06:32',
                    'updated_at' => '2019-12-03 15:06:32',
                ),
            6 =>
                array(
                    'analysis_id' => 6,
                    'field' => 'geometry',
                    'databaseName' => 'dechetteries',
                    'color_code' => '',
                    'usage' => '{"axis":"x"}',
                    'created_at' => '2019-12-03 18:14:56',
                    'updated_at' => '2019-12-03 18:14:56',
                ),
            7 =>
                array(
                    'analysis_id' => 3,
                    'field' => 'Montant',
                    'databaseName' => 'forfait_post_stationnement',
                    'color_code' => '',
                    'usage' => '{"axis":"y","statType":["sum"]}',
                    'created_at' => '2019-12-03 15:06:32',
                    'updated_at' => '2019-12-03 15:06:32',
                ),
            8 =>
                array(
                    'analysis_id' => 6,
                    'field' => 'Nb_bac_Sortant_Plastique',
                    'databaseName' => 'dechetteries_flux_entree_sortie',
                    'color_code' => '',
                    'usage' => '{"axis":"y","statType":["avg"]}',
                    'created_at' => '2019-12-03 18:14:56',
                    'updated_at' => '2019-12-03 18:14:56',
                ),
            9 =>
                array(
                    'analysis_id' => 6,
                    'field' => 'Nb_bac_Sortant_Tout-venant',
                    'databaseName' => 'dechetteries_flux_entree_sortie',
                    'color_code' => '',
                    'usage' => '{"axis":"y","statType":["avg"]}',
                    'created_at' => '2019-12-03 18:14:56',
                    'updated_at' => '2019-12-03 18:14:56',
                ),
            10 =>
                array(
                    'analysis_id' => 6,
                    'field' => 'Nb_bac_Sortant_Verre',
                    'databaseName' => 'dechetteries_flux_entree_sortie',
                    'color_code' => '',
                    'usage' => '{"axis":"y","statType":["avg"]}',
                    'created_at' => '2019-12-03 18:14:56',
                    'updated_at' => '2019-12-03 18:14:56',
                ),
            11 =>
                array(
                    'analysis_id' => 5,
                    'field' => 'nb_membres',
                    'databaseName' => 'conseil_de_quartier',
                    'color_code' => '#26a69a',
                    'usage' => '{"axis":"y"}',
                    'created_at' => '2019-12-03 15:49:06',
                    'updated_at' => '2019-12-03 15:49:06',
                ),
            12 =>
                array(
                    'analysis_id' => 5,
                    'field' => 'nb_seances',
                    'databaseName' => 'conseil_de_quartier',
                    'color_code' => '#66bae3',
                    'usage' => '{"axis":"y"}',
                    'created_at' => '2019-12-03 15:49:06',
                    'updated_at' => '2019-12-03 15:49:06',
                ),
            13 =>
                array(
                    'analysis_id' => 3,
                    'field' => 'properties.FID',
                    'databaseName' => 'quartiers',
                    'color_code' => '',
                    'usage' => '{"axis":"y","statType":["DiffSum"]}',
                    'created_at' => '2019-12-03 15:06:32',
                    'updated_at' => '2019-12-03 15:06:32',
                ),
            14 =>
                array(
                    'analysis_id' => 4,
                    'field' => 'properties.FID',
                    'databaseName' => 'quartiers',
                    'color_code' => '',
                    'usage' => '{"axis":"x"}',
                    'created_at' => '2019-12-03 15:28:48',
                    'updated_at' => '2019-12-03 15:28:48',
                ),
            15 =>
                array(
                    'analysis_id' => 5,
                    'field' => 'properties.FID',
                    'databaseName' => 'quartiers',
                    'color_code' => '',
                    'usage' => '{"axis":"x"}',
                    'created_at' => '2019-12-03 15:49:07',
                    'updated_at' => '2019-12-03 15:49:07',
                ),
            16 =>
                array(
                    'analysis_id' => 1,
                    'field' => 'properties.Shape__Area',
                    'databaseName' => 'quartiers',
                    'color_code' => '',
                    'usage' => '{"axis":"y"}',
                    'created_at' => '2019-11-26 16:51:24',
                    'updated_at' => '2019-11-26 16:51:24',
                ),
        ));


    }
}
