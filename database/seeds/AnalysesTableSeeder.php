<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnalysesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('analyses')->delete();

        DB::table('analyses')->insert(array(
            0 =>
                array(
                    'id' => 1,
                    'name' => 'Quartiers',
                    'representation_type' => 'Carte',
                    'shared' => 1,
                    'isStats' => NULL,
                    'owner_id' => 1,
                    'description' => 'Quartiers de Demopolis',
                    'body' => '{"stats":{"do_stats":false},"datasets":[{"name":"quartiers","columns":["properties.Shape__Area","geometry",null],"size":300000,"offset":0}],"joining":[]}',
                    'usage' => '{}',
                    'visibility' => 'all',
                    'theme_name' => 'Default',
                    'created_at' => '2019-11-26 16:51:24',
                    'updated_at' => '2019-11-26 16:51:24',
                    'isMap' => 1,
                ),
            1 =>
                array(
                    'id' => 2,
                    'name' => 'Age Moyen par quartier',
                    'representation_type' => 'Carte',
                    'shared' => 1,
                    'isStats' => NULL,
                    'owner_id' => 1,
                    'description' => 'Age Moyen par quartier',
                    'body' => '{"stats":{"do_stats":true,"columns":{"data":["age_moy"],"pivot":"geometry","isDate":false}},"datasets":[{"name":"quartiers","columns":["properties.FID","geometry"],"size":300000,"offset":0},{"name":"conseil_de_quartier","columns":["quartier_id","age_moy"],"size":300000,"offset":0}],"joining":[["properties.FID","quartier_id"]]}',
                    'usage' => '{}',
                    'visibility' => 'all',
                    'theme_name' => 'Default',
                    'created_at' => '2019-11-27 14:22:59',
                    'updated_at' => '2019-11-27 14:22:59',
                    'isMap' => 1,
                ),
            2 =>
                array(
                    'id' => 3,
                    'name' => 'FPS par Quartier',
                    'representation_type' => 'Carte',
                    'shared' => 1,
                    'isStats' => NULL,
                    'owner_id' => 1,
                    'description' => 'Montant des Forfaits post stationnement par quartier de Demopolis',
                    'body' => '{"stats":{"do_stats":true,"columns":{"data":["properties.FID","Montant"],"pivot":"geometry","isDate":false}},"datasets":[{"name":"quartiers","columns":["properties.FID","geometry"],"size":300000,"offset":0},{"name":"forfait_post_stationnement","columns":["id_quartier","Montant"],"size":300000,"offset":0}],"joining":[["properties.FID","id_quartier"]]}',
                    'usage' => '{}',
                    'visibility' => 'all',
                    'theme_name' => 'Finance',
                    'created_at' => '2019-12-03 15:06:32',
                    'updated_at' => '2019-12-03 15:06:32',
                    'isMap' => 1,
                ),
            3 =>
                array(
                    'id' => 4,
                    'name' => 'Conso. Hedbo Eclairage publique',
                    'representation_type' => 'Graphique en colonnes',
                    'shared' => 1,
                    'isStats' => 1,
                    'owner_id' => 1,
                    'description' => 'Consommation moyenne de l\'éclairage publique par semaine sur 6 mois',
                    'body' => '{"stats":{"do_stats":true,"columns":{"data":["consommation"],"pivot":"properties.FID","isDate":false}},"datasets":[{"name":"quartiers","columns":["properties.FID","properties.FID"],"size":300000,"offset":0},{"name":"consommation_de_leclairage_publique","columns":["id_quartier","consommation"],"size":300000,"offset":0}],"joining":[["properties.FID","id_quartier"]]}',
                    'usage' => '{}',
                    'visibility' => 'all',
                    'theme_name' => 'Default',
                    'created_at' => '2019-12-03 15:28:48',
                    'updated_at' => '2019-12-03 15:28:48',
                    'isMap' => 0,
                ),
            4 =>
                array(
                    'id' => 5,
                    'name' => 'Statistiques sur les Conseils de quartier',
                    'representation_type' => 'Graphique en colonnes',
                    'shared' => 1,
                    'isStats' => 0,
                    'owner_id' => 1,
                    'description' => 'Age moyen des membres, nombre de séances tenues et nombre de membre de chaque conseil de quartier de Demopolis',
                    'body' => '{"stats":{"do_stats":false},"datasets":[{"name":"quartiers","columns":["properties.FID","properties.FID"],"size":300000,"offset":0},{"name":"conseil_de_quartier","columns":["quartier_id","age_moy","nb_membres","nb_seances"],"size":300000,"offset":0}],"joining":[["properties.FID","quartier_id"]]}',
                    'usage' => '{}',
                    'visibility' => 'all',
                    'theme_name' => 'Default',
                    'created_at' => '2019-12-03 15:49:06',
                    'updated_at' => '2019-12-03 15:49:06',
                    'isMap' => 0,
                ),
            5 =>
                array(
                    'id' => 6,
                    'name' => 'Déchetteries connectées',
                    'representation_type' => 'Carte',
                    'shared' => 1,
                    'isStats' => NULL,
                    'owner_id' => 1,
                    'description' => 'Flux sortants des déchetteries connectées',
                    'body' => '{"stats":{"do_stats":true,"columns":{"data":["Nb_bac_Sortant_Plastique","Nb_bac_Sortant_Tout-venant","Nb_bac_Sortant_Verre"],"pivot":"geometry","isDate":false}},"datasets":[{"name":"dechetteries","columns":["properties.id","geometry"],"size":300000,"offset":0},{"name":"dechetteries_flux_entree_sortie","columns":["id_dechetterie","Nb_bac_Sortant_Plastique","Nb_bac_Sortant_Tout-venant","Nb_bac_Sortant_Verre"],"size":300000,"offset":0}],"joining":[["properties.id","id_dechetterie"]]}',
                    'usage' => '{}',
                    'visibility' => 'all',
                    'theme_name' => 'Default',
                    'created_at' => '2019-12-03 18:14:56',
                    'updated_at' => '2019-12-03 18:14:56',
                    'isMap' => 1,
                ),
        ));

    }
}
