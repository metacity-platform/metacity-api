<?php
/**
 * <API - Metacity>
 * Copyright (C) 2020.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Tests\Http\Services;

use App\direction;
use App\Http\Services\DirectionService;
use App\user;
use Illuminate\Http\Request;
use Tests\TestCase;

class DirectionServiceTest extends TestCase
{

    public function testDelDirectionService()
    {
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();

        $name = "Test";
        $desc = "Test DirectionService::delDirectionService";

        $result = direction::where("direction", $name)->first();
        $this->assertNull($result);

        $new_direction = direction::first()->replicate();
        $new_direction->direction = $name;
        $new_direction->description = $desc;
        $new_direction->save();

        $result = direction::where("direction", $name)->first()->toArray();
        $this->assertNotNull($result);

        DirectionService::delDirectionService($request, $name);

        $result = direction::where("direction", $name)->first();
        $this->assertNull($result);
    }

    public function testUpdateDirectionService()
    {
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();

        $name = "Test";
        $newName = "Test2";
        $desc = "Test DirectionService::addDirectionService";
        $newDesc = "Test2 DirectionService::addDirectionService";

        $result = direction::where("direction", $name)->first();
        $this->assertNull($result);

        $new_direction = direction::first()->replicate();
        $new_direction->direction = $name;
        $new_direction->description = $desc;
        $new_direction->save();

        $result = direction::where("direction", $name)->first()->toArray();
        $this->assertNotNull($result);

        $request["direction"] = $name;
        $request["newName"] = $newName;
        $request["desc"] = $newDesc;

        DirectionService::updateDirectionService($request);

        $result = direction::where("direction", $name)->first();
        $this->assertNull($result);

        $result = direction::where("direction", $newName)->first()->toArray();
        $this->assertNotNull($result);


        direction::where("direction", $newName)->delete();

        $result = direction::where("direction", $newName)->first();
        $this->assertNull($result);
    }

    public function testAddDirectionService()
    {
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();

        $name = "Test";
        $desc = "Test DirectionService::addDirectionService";
        $request["direction"] = $name;
        $request["desc"] = $desc;

        $result = direction::where("direction", $name)->first();
        $this->assertNull($result);

        DirectionService::addDirectionService($request);

        $result = direction::where("direction", $name)->first()->toArray();
        $this->assertNotNull($result);

        direction::where("direction", $name)->delete();

        $result = direction::where("direction", $name)->first();
        $this->assertNull($result);
    }

    public function testGetAllDirectionsService()
    {
        $result = DirectionService::getAllDirectionsService();
        $directions = direction::all();
        $this->assertEquals($result, $directions);
    }
}
