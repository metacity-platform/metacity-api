<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RepresentationTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('representation_types')->delete();

        DB::table('representation_types')->insert(array(
            0 =>
                array(
                    'name' => 'Carte',
                    'srcBegin' => 'assets/images/account/settings/statistics-map-',
                    'img' => 'assets/images/account/settings/statistics-map-light.svg',
                    'description' => 'Carte',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            1 =>
                array(
                    'name' => 'Graphique en anneau',
                    'srcBegin' => 'assets/images/account/settings/statistics-donut-',
                    'img' => 'assets/images/account/settings/statistics-donut-light.svg',
                    'description' => 'Graphique en anneau',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            2 =>
                array(
                    'name' => 'Graphique en barres',
                    'srcBegin' => 'assets/images/account/settings/statistics-bar-chart-',
                    'img' => 'assets/images/account/settings/statistics-bar-chart-light.svg',
                    'description' => 'Graphique en barres',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            3 =>
                array(
                    'name' => 'Graphique en colonnes',
                    'srcBegin' => 'assets/images/account/settings/statistics-column-',
                    'img' => 'assets/images/account/settings/statistics-column-light.svg',
                    'description' => 'Graphique en colonnes',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            4 =>
                array(
                    'name' => 'Graphique en courbes',
                    'srcBegin' => 'assets/images/account/settings/statistics-line-',
                    'img' => 'assets/images/account/settings/statistics-line-light.svg',
                    'description' => 'Graphique en courbes',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            5 =>
                array(
                    'name' => 'Graphique en radar',
                    'srcBegin' => 'assets/images/account/settings/statistics-radar-pentagon-',
                    'img' => 'assets/images/account/settings/statistics-radar-pentagon-light.svg',
                    'description' => 'Graphique en radar',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            6 =>
                array(
                    'name' => 'Graphique en secteur',
                    'srcBegin' => 'assets/images/account/settings/statistics-camembert-',
                    'img' => 'assets/images/account/settings/statistics-camembert-light.svg',
                    'description' => 'Graphique en secteur',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            7 =>
                array(
                    'name' => 'Graphique polaire',
                    'srcBegin' => 'assets/images/account/settings/statistics-radar-polaire-',
                    'img' => 'assets/images/account/settings/statistics-radar-polaire-light.svg',
                    'description' => 'Graphique polaire',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            8 =>
                array(
                    'name' => 'Somme éléments',
                    'srcBegin' => 'assets/images/account/settings/statistics-the-sum-of-mathematical-symbol-',
                    'img' => 'assets/images/account/settings/statistics-the-sum-of-mathematical-symbol-light.svg',
                    'description' => 'Somme éléments',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            9 =>
                array(
                    'name' => 'Tableau',
                    'srcBegin' => 'assets/images/account/settings/statistics-table-',
                    'img' => 'assets/images/account/settings/statistics-table-light.svg',
                    'description' => 'Tableau de données',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
        ));


    }
}
