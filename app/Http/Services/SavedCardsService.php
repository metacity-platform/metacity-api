<?php

/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** @noinspection PhpUndefinedFieldInspection */

namespace App\Http\Services;


use App\analysis;
use App\analysis_column;
use App\saved_card;
use Illuminate\Http\Request;

class SavedCardsService
{
    public static function getAllSavedCardsService(Request $request)
    {
        $user = $request->get('user');
        $saved_cards = saved_card::where('user_id', $user->user_id)->get();
        foreach ($saved_cards as $saved_card) {
            $analysis = analysis::where('id', $saved_card->id)->first();
            $analysis->analysis_column = analysis_column::where('analysis_id', $saved_card->id)->get();
            $saved_card->analysis = $analysis;
        }

        return $saved_cards;
    }

    public static function saveCardService(Request $request)
    {
        $user = $request->get('user');

        $card = new saved_card();
        $card->id = $request->get('id');
        $card->user_id = $user->user_id;
        $card->position = saved_card::where('user_id', $user->user_id)->count();
        $card->size = $request->get('size');
        $card->displayed = null != $request->get('displayed') ? $request->get('displayed') : false;

        $card->save();

        $CachingService = new  CachingService(null);
        $CachingService->checkCount();
        $CachingService->setCaching((integer)$request->get('id'));

        return $card;
    }

    public static function updateCardService(Request $request)
    {
        $user = $request->get('user');

        if ($request->get('position') == null and $request->get('displayed') == true) {
            $position = saved_card::where("position", null)->max('position') + 1;
        } else {
            $position = $request->get('position');
        }

        $card = saved_card::where('user_id', $user->user_id)->where('id', $request->get('id'))->first();
        $card->id = $request->get('id');
        $card->position = $position;
        $card->size = $request->get('size');
        $card->displayed = null != $request->get('displayed') ? $request->get('displayed') : $card->displayed;

        $card->save();

        return $card;
    }

    public static function deleteCardService(Request $request, $id)
    {
        $user = $request->get('user');

        $card = saved_card::where('user_id', $user->user_id)->where('id', $id);
        $position = $card->get('position')[0]['position'];
        $card->delete();
        saved_card::where('position', ">", $position)->decrement('position', 1);
    }
}
