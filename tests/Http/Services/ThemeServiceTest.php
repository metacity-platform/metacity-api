<?php
/**
 * <API - Metacity>
 * Copyright (C) 2020.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Tests\Http\Services;


use App\Http\Services\ThemeService;
use App\theme;
use App\user;
use Illuminate\Http\Request;
use Tests\TestCase;

class ThemeServiceTest extends TestCase
{

    public function testAddThemeService()
    {
        $name = "Test";
        $description = "Theme de test";
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();
        $request["name"] = $name;
        $request["description"] = $description;

        $result = theme::where("name", $name)->where('description', $description)->get();
        $this->assertEmpty($result);

        ThemeService::addThemeService($request);
        $result = theme::where("name", $name)->where('description', $description)->get();
        $this->assertNotEmpty($result);

        theme::where("name", $name)->where('description', $description)->delete();
        $result = theme::where("name", $name)->where('description', $description)->get();

        $this->assertEmpty($result);
    }

    public function testUpdateThemeService()
    {
        $name = "Test";
        $newName = "Test2";
        $description = "Theme de test";
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();
        $request["name"] = $name;
        $request["newName"] = $newName;
        $request["desc"] = $description;

        $result = theme::where("name", $name)->where('description', $description)->get();
        $this->assertEmpty($result);

        $theme = new Theme();
        $theme->name = $name;
        $theme->description = $description;
        $theme->save();
        $result = theme::where("name", $name)->where('description', $description)->get();
        $this->assertNotEmpty($result);

        ThemeService::updateThemeService($request);
        $result = theme::where("name", $newName)->where('description', $description)->get();
        $this->assertNotEmpty($result);
        $result = theme::where("name", $name)->where('description', $description)->get();
        $this->assertEmpty($result);

        theme::where("name", $newName)->where('description', $description)->delete();
        $result = theme::where("name", $newName)->where('description', $description)->get();

        $this->assertEmpty($result);
    }

    public function testGetAllThemesService()
    {
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();
        $result = ThemeService::getAllThemesService();
        $themes = theme::get();
        $this->assertEquals($themes, $result);
    }

    public function testDeleteThemeService()
    {
        $name = "Test";
        $newName = theme::first()["name"];
        $description = "Theme de test";
        $request = new Request();
        $request["user"] = user::where('user_id', 1)->first();


        $result = theme::where("name", $name)->where('description', $description)->get();
        $this->assertEmpty($result);

        $theme = new Theme();
        $theme->name = $name;
        $theme->description = $description;
        $theme->save();
        $result = theme::where("name", $name)->where('description', $description)->get();
        $this->assertNotEmpty($result);

        ThemeService::deleteThemeService($request, $name, $newName);
        $result = theme::where("name", $name)->where('description', $description)->get();

        $this->assertEmpty($result);
    }
}
