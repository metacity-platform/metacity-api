<?php
/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** @noinspection PhpUndefinedFieldInspection */
/** @noinspection PhpUnused */

namespace App\Http\Controllers;


use App\dataset;
use App\Http\Services\DatasetService;
use App\user;
use Illuminate\Http\Request;


class DatasetController extends Controller
{
    public function updateDataset(Request $request)
    {
        DatasetService::updateDatasetService($request);
        return response("", 200);
    }


    public function uploadDataset(Request $request)
    {
        $fields = DatasetService::uploadDatasetService($request);
        return response($fields, 200);
    }

    public function getDatasetsToValidate(Request $request)
    {
        $data = DatasetService::getDatasetsToValidateService($request);
        return response($data)->header('Content-Type', 'application/json')
            ->header('charset', 'utf-8');
    }

    public function getFilterDatasets(Request $request)
    {
        return DatasetService::getFilterDatasetsService($request);
    }

    public function getRepresentationsOfDataset($id)
    {
        $representations = DatasetService::getRepresentationsOfDatasetService($id);
        return response($representations)->header('Content-Type', 'application/json')
            ->header('charset', 'utf-8');

    }

    public function getAllColumnFromDataset(Request $request, $id)
    {
        $columns = DatasetService::getAllColumnFromDatasetService($request, $id);
        return response($columns)->header('Content-Type', 'application/json')
            ->header('charset', 'utf-8');
    }

    public function getAllAccessibleColumnsFromADataset(Request $request, Dataset $dataset)
    {
        return DatasetService::getAllAccessibleColumnsFromADatasetService($request, $dataset);
    }

    public function saveDataset(Request $request, $id)
    {
        DatasetService::saveDatasetService($request, $id);
        return response("", 200);
    }

    public function saveAndFavoriteDataset(user $user, dataset $dataset, $favorite = false)
    {
        DatasetService::saveAndFavoriteDatasetService($user, $dataset, $favorite);
        return response("", 200);
    }

    public function favoriteDataset(Request $request, $id)
    {
        DatasetService::favoriteDatasetService($request, $id);
        return response("", 200);
    }

    public function unSaveDataset(Request $request, $id)
    {
        DatasetService::unSaveDatasetService($request, $id);
        return response("", 200);
    }

    public function getDatasetById(Request $request, $id)
    {
        $data = DatasetService::getDatasetByIdService($request, $id);
        return response($data)->header('Content-Type', 'application/json')->header('charset', 'utf-8');
    }

    public function getDatasetsSize(Request $request, $type)
    {
        return DatasetService::getDatasetsSizeService($request, $type);
    }

    public function getAllDatasets(Request $request, bool $count = false)
    {
        $data = DatasetService::getAllDatasetsService($request, $count);
        return response($data)->header('Content-Type', 'application/json')
            ->header('charset', 'utf-8');
    }

    public function getAllAccessibleSavedDatasets(Request $request, bool $count = false)
    {
        $data = DatasetService::getAllAccessibleSavedDatasetsService($request, $count);
        return response($data)->header('Content-Type', 'application/json')
            ->header('charset', 'utf-8');
    }

    public function getAllAccessibleFavoriteDatasets(Request $request, bool $count = false)
    {
        $data = DatasetService::getAllAccessibleFavoriteDatasetsService($request, $count);
        return response($data)->header('Content-Type', 'application/json')
            ->header('charset', 'utf-8');
    }

    public function unValidate(Request $request, $id)
    {
        DatasetService::unValidateService($request, $id);
        return response("", 200);
    }
}
