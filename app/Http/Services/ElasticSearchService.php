<?php
/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace App\Http\Services;


use Illuminate\Http\Request;

class ElasticSearchService
{
    private $start_date;
    private $date_col;
    private $end_date;
    private $week_day;
    private $start_minute;
    private $end_minute;
    private $match;
    private $sort;


    public function __construct(Request $request)
    {
        $this->date_col = $request->get('date_col');
        $this->start_date = $request->get('start_date');
        $this->end_date = $request->get('end_date');
        $this->week_day = $request->get('weekdays');
        $this->start_minute = $request->get('start_minute');
        $this->end_minute = $request->get('end_minute');
        $this->match = $request->get("match");
        $this->sort = $request->get("sort");
    }

    public function getMinuteFilter()
    {
        if ($this->start_minute !== null && $this->end_minute !== null) {
            return "(doc['" . $this->date_col . "'].value.getMinuteOfDay() >= " . $this->start_minute . " && doc['" . $this->date_col . "'].value.getMinuteOfDay() < " . $this->end_minute . ")";
        } else {
            return null;
        }

    }

    public function getWeekdayFilter()
    {
        $emptyDayQuery = "doc['" . $this->date_col . "'].value.dayOfWeek == ";
        $fullDayQuery = "";
        if ($this->week_day != null) {
            foreach ($this->week_day as $day) {
                $fullDayQuery .= $emptyDayQuery . $day . " || ";
            }
            $fullDayQuery = str_replace(" || )", ")", "(" . $fullDayQuery . ")");
            return $fullDayQuery;
        } else {
            return null;
        }
    }

    public function getMatchFilter()
    {
        if ($this->match != null) {
            $match = explode("=", $this->match);
            return ["match" => [$match[0] => $match[1]]];
        }
        return null;
    }

    public function getSortFilter()
    {
        if ($this->sort != null) {
            $sort = [];
            foreach (array_keys($this->sort) as $key) {
                if ($this->sort[$key]) {
                    $direction = "asc";
                } else {
                    $direction = "desc";
                }
                array_push($sort, [$key => $direction]);
            }
            return $sort;
        }
        return null;
    }

    public function getFilter(Array $body, $minuteQuery, $fullDayQuery, $matchQuery, $sortQuery)
    {
        if ($this->date_col != null or $matchQuery != null or $sortQuery != null) {
            if ($sortQuery != null) {
                $body = ['sort' => $sortQuery, "query" => ["bool" => ["must" => []]]];
            } else {
                $body = ['sort' => [[$this->date_col => ['order' => 'desc']]], "query" => ["bool" => ["must" => []]]];
            }

            if ($this->date_col != null && $this->start_date != null && $this->end_date == null) {
                array_push($body["query"]["bool"]["must"], ['range' => [$this->date_col => ['gte' => $this->start_date, 'lte' => $this->start_date]]]);
            } elseif ($this->date_col != null && $this->start_date != null && $this->end_date != null) {
                array_push($body["query"]["bool"]["must"], ['range' => [$this->date_col => ['gte' => $this->start_date, 'lte' => $this->end_date]]]);
            }

            if ($fullDayQuery != null && ($minuteQuery != null)) {
                $body["query"]["bool"]["filter"] = [['script' => ['script' => "(" . $fullDayQuery . " && " . $minuteQuery . ")"]]];
            } elseif ($fullDayQuery != null && ($minuteQuery == null)) {
                $body["query"]["bool"]["filter"] = [['script' => ['script' => $fullDayQuery]]];
            } elseif ($fullDayQuery == null && ($minuteQuery != null)) {
                $body["query"]["bool"]["filter"] = [['script' => ['script' => $minuteQuery]]];
            }

            if ($matchQuery != null) {
                array_push($body["query"]["bool"]["must"], $matchQuery);
            }
        }

        return $body;
    }
}
