<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SavedCardsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('saved_cards')->delete();

        DB::table('saved_cards')->insert(array(
            0 =>
                array(
                    'id' => 3,
                    'user_id' => 1,
                    'position' => 1,
                    'displayed' => 1,
                    'size' => 2,
                    'created_at' => '2019-12-03 15:06:32',
                    'updated_at' => '2019-12-03 15:39:57',
                ),
            1 =>
                array(
                    'id' => 4,
                    'user_id' => 1,
                    'position' => 1,
                    'displayed' => 1,
                    'size' => 3,
                    'created_at' => '2019-12-03 15:28:48',
                    'updated_at' => '2019-12-03 15:46:11',
                ),
            2 =>
                array(
                    'id' => 5,
                    'user_id' => 1,
                    'position' => NULL,
                    'displayed' => 0,
                    'size' => NULL,
                    'created_at' => '2019-12-03 15:49:07',
                    'updated_at' => '2019-12-03 15:49:07',
                ),
            3 =>
                array(
                    'id' => 6,
                    'user_id' => 1,
                    'position' => 1,
                    'displayed' => 1,
                    'size' => 4,
                    'created_at' => '2019-12-03 18:14:56',
                    'updated_at' => '2019-12-03 18:15:25',
                ),
        ));


    }
}
