<?php
/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

/**
 * @property mixed name
 * @property mixed description
 */
class theme extends Model
{
    public $incrementing = false;
    protected $table = 'themes';
    protected $primaryKey = 'name';
    protected $keyType = 'string';

    private $rules = array(
        'name' => 'required|min:3|alpha',
        'description' => 'required|min:3',
    );

    public function validate($data)
    {
        // make a new validator object
        $v = Validator::make($data, $this->rules);
        // return the result
        if ($v->fails()) {
            error_log(implode($v->errors()->all()));
            return false;
        }
        return true;
    }

    public function analysis()
    {
        return $this->belongsToMany('App\analysis', 'analysis_theme', 'name', 'id');
    }
}
