<?php
/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** @noinspection PhpUnused */
/** @noinspection PhpUndefinedClassInspection */

namespace App\Http\Controllers;


use App\Http\Services\IndexService;
use Illuminate\Http\Request;


class IndexController extends Controller
{
    public function getFieldsAndType(Request $request, $name)
    {
        $fields = IndexService::getFieldsAndTypeService($request, $name);
        return response($fields, 200);
    }

    public function getAllIndex()
    {
        $indexes = IndexService::getAllIndexService();
        return response($indexes)->header('Content-Type', 'application/json')->header('charset', 'utf-8');
    }

    public function getAllDateFieldsFromAnIndexFromItsName(Request $request, $name)
    {
        $date_fields = IndexService::getAllDateFieldsFromAnIndexFromItsNameService($request, $name);
        return response($date_fields, 200);
    }

    public function getAllFieldsFromIndexByName(Request $request, $name)
    {
        $result = IndexService::getAllFieldsFromIndexByNameService($request, $name);
        return response($result, 200);
    }

    public function getAllAccessibleFieldsFromIndexByName(Request $request, $name)
    {
        $result = IndexService::getAllAccessibleFieldsFromIndexByNameService($request, $name);
        return response($result, 200);
    }

    public function getIndexByName(Request $request, $name, $quantity = 5, $offset = 0, $date_col = null, $start_date = null, $end_date = null)
    {
        $data = IndexService::getIndexByNameService($request, $name, $quantity, $offset, $date_col, $start_date, $end_date);
        return response($data)->header('Content-Type', 'application/json')->header('charset', 'utf-8');
    }

    public function getIndexByNameQuantityAndOffset(Request $request, $name, $quantity = 5, $offset = 0, $date_col = null, $start_date = null, $end_date = null)
    {
        $result = IndexService::getIndexByNameQuantityAndOffsetService($request, $name, $quantity, $offset, $date_col, $start_date, $end_date);
        return response($result, 200);
    }

    public function getIndexFromCoordinatesInShape(Request $request)
    {
        $filtered_data = IndexService::getIndexFromCoordinatesInShapeService($request);
        return response($filtered_data)->header('Content-Type', "application/json")->header('charset', 'utf-8');
    }

    public function getLiteIndex(Request $request)
    {
        return IndexService::liteIndexService($request);
    }

    public function join(Request $request)
    {
        return IndexService::joinService($request);
    }

    public function getInPointInPolygon(Request $request)
    {
        return IndexService::getInPointInPolygonService($request);
    }

    public function getLast(Request $request)
    {
        return IndexService::getLast($request);
    }

    public function deleteIndex(Request $request, $id)
    {
        IndexService::deleteIndexService($request, $id);
        return response("done", 200);
    }

    public function countElementInIndex($name)
    {
        return IndexService::countElementInIndexService($name);
    }
}
