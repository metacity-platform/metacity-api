<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServicesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('services')->delete();

        DB::table('services')->insert(array(
            0 =>
                array(
                    'service' => 'Finance',
                    'description' => 'Service finance',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            1 =>
                array(
                    'service' => 'N/A',
                    'description' => 'N/A',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            2 =>
                array(
                    'service' => 'Numérique',
                    'description' => 'Service du numérique',
                    'created_at' => '2019-11-26 12:31:46',
                    'updated_at' => '2019-11-26 12:31:46',
                ),
        ));


    }
}
