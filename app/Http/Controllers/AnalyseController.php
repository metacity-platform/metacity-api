<?php
/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** @noinspection PhpUnused */

namespace App\Http\Controllers;

use App\Http\Services\AnalyseService;
use Illuminate\Http\Request;


class AnalyseController extends Controller
{
    public function saveAnalyse(Request $request)
    {
        return response(AnalyseService::saveAnalyseService($request))->header('Content-Type',
            'application/json')->header('charset', 'utf-8');
    }

    public function createAnalysisColumn($analysis_columns, $id)
    {
        AnalyseService::createAnalysisColumnService($analysis_columns, $id);
        return response("", 200);
    }

    public function deleteAnalysis(Request $request, $id)
    {
        AnalyseService::deleteAnalysisService($request, $id);
        return response("", 200);
    }

    public function getAnalysisById(Request $request, $id)
    {
        return AnalyseService::getAnalysisByIdService($request, $id);
    }

    public function getAllAccessibleAnalysis(Request $request, bool $saved = false, $id = null)
    {
        return AnalyseService::getAllAccessibleAnalysisService($request, $saved, $id);
    }

    public function getAllSavedAnalysis(Request $request)
    {
        return AnalyseService::getAllSavedAnalysisService($request);
    }
}
