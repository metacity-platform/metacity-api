<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('roles')->delete();

        DB::table('roles')->insert(array(
            0 =>
                array(
                    'role' => 'Administrateur',
                    'description' => 'Utilisateur d\'administration de la plateforme',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            1 =>
                array(
                    'role' => 'Désactivé',
                    'description' => 'Utilisateur désactivé de la plateforme',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            2 =>
                array(
                    'role' => 'Référent-Métier',
                    'description' => 'Utilisateur metier de la plateforme',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
            3 =>
                array(
                    'role' => 'Utilisateur',
                    'description' => 'Utilisateur de la plateforme',
                    'created_at' => '2019-11-26 10:26:26',
                    'updated_at' => '2019-11-26 10:26:26',
                ),
        ));


    }
}
