<?php
/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace App\Http\Services;


use App\auth_users;
use App\colauth_users;
use App\column;
use App\dataset;
use App\user;
use App\user_saved_dataset;
use App\user_theme;
use Illuminate\Http\Request;

class AccessibleDatasetsService
{
    public static function getAllAccessibleDatasets(Request $request, user $user = null, bool $validate = false, bool $saved = false, bool $favorite = false, int $id = null,
                                                    bool $count = false, int $offset = null, int $size = null)
    {
        if ($user == null) {
            $user = $request->get('user');
        }

        $datasets = dataset::where(function ($query) use ($user) {
            if ($user["role"] == "Administrateur") {
                $query->get();
            } else {
                $userThemes = ObjectListToArrayService::getObjectListToArray(user_theme::where("user_id", $user["user_id"])->get("name"));

                $idColumnsByAuth = ObjectListToArrayService::getObjectListToArray(colauth_users::where("user_id", $user["user_id"])->get("id"), "id");
                $idColumnsByVisibility = ObjectListToArrayService::getObjectListToArray(column::where(function ($query) use ($user, $userThemes) {
                    $query->where("visibility", "all");
                    $query->orWhere("visibility", "worker")->whereIn("themeName", $userThemes);
                    if ($user["role"] == "Référent-Métier") {
                        $query->orWhere("visibility", "job_referent")->whereIn("themeName", $userThemes);
                    }
                })->get("id"), "id");

                $idDatasetByAuth = ObjectListToArrayService::getObjectListToArray(auth_users::where("user_id", $user["user_id"])->get("id"), "id");
                $idDatasetByIdColumns = ObjectListToArrayService::getObjectListToArray(column::whereIn("id", array_merge_recursive($idColumnsByAuth, $idColumnsByVisibility))->get("dataset_id"), "dataset_id");

                $query->whereIn("id", array_merge_recursive($idDatasetByAuth, $idDatasetByIdColumns));
                $query->orWhere("visibility", "all");
                $query->orWhere("visibility", "worker")->whereIn("themeName", $userThemes);
                if ($user["role"] == "Référent-Métier") {
                    $query->orWhere("visibility", "job_referent")->whereIn("themeName", $userThemes);
                }
            }
        })
            ->where("validated", !$validate)
            ->where(function ($query) use ($user, $saved, $favorite, $id) {
                if ($saved) {
                    $query->whereIn("id", ObjectListToArrayService::getObjectListToArray(user_saved_dataset::where("user_id", $user["user_id"])
                        ->where("favorite", $favorite)->get("id"), "id"));
                }
                if ($id != null) {
                    $query->where("id", $id);
                }
            });

        if ($request["themeName"] != null) {
            $datasets->whereIn("themeName", $request["themeName"]);
        }

        if (key_exists("geojson", $request->toArray())) {
            $datasets->where("GEOJSON", (integer)$request["geojson"]);
        }

        if ($count) {
            $datasets = $datasets->get()->count();
        } else {
            if ($size != null) {
                $datasets = $datasets->offset($offset)->limit($size)->get();
            } else {
                $datasets = $datasets->offset($offset)->limit(10000000)->get();
            }
        }

        foreach ($datasets as $dataset) {
            $id = $dataset["id"];
            $value = user_saved_dataset::where('id', $id)->where("user_id", $user["user_id"])->get('favorite')->first();
            if ($value !== null) {
                $dataset["favorite"] = $value;
                $dataset["saved"] = (integer)!$value;
            } else {
                $dataset["favorite"] = 0;
                $dataset["saved"] = 0;
            }
        }

        return $datasets;
    }

    public static function getAllAccessibleColumnsFromADatasetService(Request $request, Dataset $dataset)
    {
        $user = $request->get('user');
        $role = $user->role;

        $themes = ObjectListToArrayService::getObjectListToArray(user_theme::where("user_id", $user["user_id"])->get("name"));

        switch ($role) {
            case "Administrateur":
                $columns = column::where('dataset_id', $dataset->id)->get();
                break;
            case "Référent-Métier":
                $columns = column::where('dataset_id', $dataset->id)->whereIn('themeName', $themes)->whereIn('visibility', ['job_referent', 'worker'])->get();
                $columns->merge(column::where('dataset_id', $dataset->id)->whereIn('visibility', ['all', null]));
                break;
            case "Utilisateur":
                $columns = column::where('dataset_id', $dataset->id)->where('visibility', 'worker')->where('themeName', $themes)->get();
                $columns->merge(column::where('dataset_id', $dataset->id)->whereIn('visibility', ['all', null]));
                break;
            default:
                $columns = [];
                break;
        }
        $array = [];
        $directColumns = $user->columns;
        foreach ($directColumns as $dc) {
            if ($dc->dataset_id == $dataset->id) {
                array_push($array, $dc);
            }
        }
        $columns = $columns->merge($array);
        return $columns;
    }
}
