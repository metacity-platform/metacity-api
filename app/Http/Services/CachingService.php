<?php

/**
 * <API - Metacity>
 * Copyright (C) 2019.  <Metapolis>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/** @noinspection PhpUndefinedFieldInspection */

namespace App\Http\Services;

use App\analysis;
use App\caching;
use Carbon\Carbon;
use Exception;

class CachingService
{
    /**
     * @var string
     */
    private $key;

    public function __construct($body)
    {
        $this->key = $body;
    }

    public function getCaching()
    {
        $result = caching::where("body", $this->key)->get();
        if ($result != "[]") {
            return true;
        }
        return false;
    }

    public function setCaching($id)
    {
        $body = analysis::where("id", $id)->get()[0]["body"];
        if (!caching::where("body", $body)->first()) {
            $catching = new caching();
            $body = preg_replace("#\n|\t|\r| |#", "", $body);
            $catching->body = $body;
            $catching->result = "-1";
            $catching->save();
        }
    }

    public function getResult()
    {
        $result = caching::where("body", $this->key)->get("result")[0]["result"];
        if ($result == "-1") {
            return false;
        }
        return $result;
    }

    public function setResult($result)
    {
        try {
            caching::where("body", $this->key)->update(['result' => json_encode($result)]);
        } catch (Exception $exception) {
            unset($exception);
        }
    }

    public function checkCount()
    {
        if (caching::count() + 1 > 30) {
            caching::orderBy('updated_at', 'asc')->first()->delete();
        }
    }

    public function checkAge()
    {
        caching::where('body', $this->key)
            ->whereDate('updated_at', "<", Carbon::now()->subWeek())
            ->update(["result" => "-1"]);
    }
}
